package com.PendingDenial.test;
import static com.PendingDenial.test.DriverScript.APP_LOGS;
import static com.PendingDenial.test.DriverScript.CONFIG;
import static com.PendingDenial.test.DriverScript.OR;




//import static com.qtpselenium.test.DriverScript.currentTestSuiteXLS;
//import static com.qtpselenium.test.DriverScript.currentTestCaseName;
//import static com.qtpselenium.test.DriverScript.currentTestDataSetID;




import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
//import java.io.File;
//import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;





import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.SystemClock;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.interactions.Actions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;




//import jxl.Cell;
/*import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
*/
import com.PendingDenial.test.Constants;
import com.PendingDenial.xls.read.Xls_Reader;



public class Keywords {
	public Xls_Reader WebFileXLS;
	public Xls_Reader VerbalFileXLS;
	public WebDriver driver;
	public  FileInputStream fileInStr = null;
	public  FileOutputStream fileOutStr =null;
	public XSSFWorkbook XLworkbook = null;
	public XSSFSheet XLsheet = null;
	public XSSFRow XLrow   =null;
	public XSSFCell XLcell = null;
	String CurrentURL;
	int CurrentRowWeb=2;
	int CurrentRowVer=2;
	int WebrowNumGlobal=0;
	int VerrowNumGlobal=0;
	int ReqHTMLIndex=0;
	int CurrentRowTestCase=2;
	String TestCaseName="";
	Date CCTimeTravelDate;
	public Actions action;
			
		public String openBrowser(String object,String data){  
			  APP_LOGS.debug("Opening browser");
			  if(CONFIG.getProperty("browserType").equals("Mozilla"))
			   driver=new FirefoxDriver();
			  else if(CONFIG.getProperty("browserType").equals("IE"))
			   {
			   System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+("\\IEDriverServer.exe"));
			   DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
			   dc.setCapability("nativeEvents", false);

			   driver=new InternetExplorerDriver(dc);
			   }
			  else if(CONFIG.getProperty("browserType").equals("Chrome"))
			  { System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+("\\chromedriver.exe"));

			   driver=new ChromeDriver();
			  }
			  long implicitWaitTime=Long.parseLong(CONFIG.getProperty("implicitwait"));
			  driver.manage().timeouts().implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
			  driver.manage().window().maximize();
			  return Constants.KEYWORD_PASS;

			 }
	
	public String navigate(String object,String data){  
		  APP_LOGS.debug("Navigating to URL");
		  try{
		  
		  driver.navigate().to(data);
		  }catch(Exception e){
		   return Constants.KEYWORD_FAIL+" -- Not able to navigate";
		  }
		  return Constants.KEYWORD_PASS;
		 }
	
	public String clickLink(String object,String data){
        APP_LOGS.debug("Clicking on link ");
        try{
        driver.findElement(By.xpath(OR.getProperty(object))).click();
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link"+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS;
	}

	
	public  String writeInInputFromColById(String object,String data){
		APP_LOGS.debug("Writing in text box");
		try{
			driver.findElement(By.id(OR.getProperty(object))).sendKeys(data);
			}
			catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
			}
		return Constants.KEYWORD_PASS;
		
		
	}
	
	public String verifyTextById(String object, String data){
		APP_LOGS.debug("Verifying the text");
		try{
			String actual=driver.findElement(By.id(OR.getProperty(object))).getText();
			String expected=data;
	    	if(actual.equals(expected))
	    		return Constants.KEYWORD_PASS;
	    	else
	    	
	    	{
//	    		System.out.println("text not verified"+"--actualText->>"+actual+"--expectedText->>"+expected);
	    		return Constants.KEYWORD_FAIL+"<-->text not verified"+"--actualText->>"+actual+"--expectedText->>"+expected;
	    	}
			}catch(Exception e){
				return Constants.KEYWORD_FAIL+" Object not found "+e.getMessage();
			}
	}
	
	public String verifyTextByXpath(String object, String data){
		APP_LOGS.debug("Verifying the text");
		try{
			String actual=driver.findElement(By.xpath(OR.getProperty(object))).getText();
			String expected=data;
	    	    	    	
	    	if(actual.equals(expected))
	    		return Constants.KEYWORD_PASS;
	    	else
	    		return Constants.KEYWORD_FAIL+"<-->text not verified"+"--actual->>"+actual+"--expected->>"+expected;
			}catch(Exception e){
				return Constants.KEYWORD_FAIL+" Object not found "+e.getMessage();
				}
	}
		
		public String verifyTextContainedByXpath(String object, String data){
			APP_LOGS.debug("Verifying the text");
			try{
				String actual=driver.findElement(By.xpath(OR.getProperty(object))).getText();
				String expected=data;
		    	    	    	
		    	if(actual.contains(expected))
		    		return Constants.KEYWORD_PASS;
		    	else
		    		return Constants.KEYWORD_FAIL+"<-->The test is not conatined in the link"+"--actual->>"+actual+"--expected->>"+expected;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+" Object not found "+e.getMessage();
				}
	}
	
		
	public  String verifyTitle(String object, String data){
	       APP_LOGS.debug("Verifying title");
	       try{
	    	   String actualTitle= driver.getTitle();
	    	   String expectedTitle=OR.getProperty(object);
	    	   if(actualTitle.equals(expectedTitle))
		    		return Constants.KEYWORD_PASS;
		    	else
		    		return Constants.KEYWORD_FAIL+" -- Title not verified "+expectedTitle+" -- "+actualTitle;
			   }catch(Exception e){
					return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

			}		
	}
	
	public  String waitTillTextVisibilityByID(String object,String data){
		  APP_LOGS.debug("Waiting for an element to be visible");
		  try{
		   		  String buf=driver.findElement(By.id(OR.getProperty(object))).getText(); 
		   		  int k=0;
		   		  while(buf!=null && k<600)
		   		  {
		   			  Thread.sleep(100L);
		   			  k=k+1;
		   		  }
		  }
		  catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to wait "+e.getMessage();
		  }
		return Constants.KEYWORD_PASS;
	}
	
	
	public  String writeInInputByXpath(String object,String data){
		APP_LOGS.debug("Writing in text box");
		
		try{
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(data);
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

		}
		return Constants.KEYWORD_PASS;
		
	}

	public  String pause(String object,String data){
		APP_LOGS.debug("pause");
		
		try{
			Thread.sleep(5000L);
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to wait "+e.getMessage();

		}
		return Constants.KEYWORD_PASS;
		
	}
	
	public  String pauseForNsec(String object,String data){
		APP_LOGS.debug("pause");
		
		try{
			int k = Integer.parseInt(data.split("k")[0]);
			Thread.sleep((long)(k*1000));
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to wait "+e.getMessage();

		}
		return Constants.KEYWORD_PASS;
		
	}
	
	public  String closeBrowser(String object, String data){
		APP_LOGS.debug("Closing the browser");
		try{
			driver.quit();
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+"Unable to close browser. Check if its open"+e.getMessage();
		}
		return Constants.KEYWORD_PASS;

	}
	
	 
	 public String switchToDefaultContent(String object,String data){
	        APP_LOGS.debug("Switching to default content in HTML");
	        try{
	        	driver.switchTo().defaultContent(); 
	        }
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Unable to switch to default content"+e.getMessage();
	        }
	        return Constants.KEYWORD_PASS;
			
		}


	public String selectFrame(String object,String data){
        APP_LOGS.debug("Select an Iframe with dynamically generated ID ");
        try{
        	String abc = driver.findElement(By.tagName("iframe")).getAttribute("id");
        	driver.switchTo().frame(abc);
        	  
            

        }
        catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to select frame "+e.getMessage();
        }
        return Constants.KEYWORD_PASS;
		
	}
	public  String writeRandomAcntNo(String object,String data){
		  APP_LOGS.debug("Writing random integer in text box for last 4 digits of account no");
		  
		  try{
			  String randNumber=Integer.toString((int)(1000+(Math.random()*8999))); 
			  driver.findElement(By.id(OR.getProperty(object))).click();
			  driver.findElement(By.id(OR.getProperty(object))).sendKeys(randNumber);
			  Constants.AccountNo = randNumber;
			  return Constants.KEYWORD_PASS;
		  }catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		  }		  
	}
	
	public  String writeRandomPin(String object,String data){
		  APP_LOGS.debug("Writing random integer in text box for  the pincode of the referrer/referee");
		  try{
			  String randNumber=Integer.toString((int)(10000+(Math.random()*89999))); 
			  driver.findElement(By.id(OR.getProperty(object))).click();
			  driver.findElement(By.id(OR.getProperty(object))).sendKeys(randNumber);
			  Constants.Pin = randNumber;
			  return Constants.KEYWORD_PASS;
		  }catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		  }		  
	}
	
	 public  String changeFocusUp(String object,String data){
		 APP_LOGS.debug("Changing the focus to top window ");
		    
		    try{
		     // considering that there is only one tab opened in that point.
		        String oldTab = driver.getWindowHandle();
		        ArrayList<String> oldList = new ArrayList<String>(driver.getWindowHandles());
		       
		        int CurrentNoOfHandle= driver.getWindowHandles().size();
		        driver.findElement(By.xpath(OR.getProperty(object))).click();
		        waitTillNoOfWindows(CurrentNoOfHandle+1);
		        ArrayList<String> newList = new ArrayList<String>(driver.getWindowHandles());
		        
		        for(int i=0; i<oldList.size();i++)
		        {
		         for(int j=0; j<newList.size(); j++)
		         {
		          if(oldList.get(i).equals(newList.get(j)))
		           newList.remove(j);
		         }
		        }
		        
		       
		        driver.switchTo().window(newList.get( 0 ) );
		        Constants.OldTab=oldTab;      
		        
		    }
		    catch(Exception e){
		     return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
		     }
		    return Constants.KEYWORD_PASS;
		  }
	 
	 public String SwitchToWindow(String object, String data){
		   APP_LOGS.debug("Switching to new window");
		   try{ 
		  String Parenthandle=driver.getWindowHandle();
		  APP_LOGS.debug("ParentWindowHandle is" + Parenthandle );
		  APP_LOGS.debug("Driver is in the following url" +driver.getCurrentUrl());
		  Set<String> HandlesBeforeClick=driver.getWindowHandles();
		  int CurrentNoOfHandle=HandlesBeforeClick.size();
		   driver.findElement(By.xpath(OR.getProperty(object))).click();
		   APP_LOGS.debug("Button is clicked");		   
		   waitTillNoOfWindows(CurrentNoOfHandle+1);
		   Set<String> handle=driver.getWindowHandles();
		   APP_LOGS.debug("total number of handles= "+ handle);
		   
		   if(handle.contains(Parenthandle))
		   handle.remove(Parenthandle);
		   
		   APP_LOGS.debug("no. of handles after removing parent handle"+ handle);
		   for(String winHandle : handle){  
		    APP_LOGS.debug("window handle after"+ winHandle );
		    driver.switchTo().window(winHandle);
		    APP_LOGS.debug("Second window handle="+ winHandle);
		   
		  }
		     APP_LOGS.debug("Switch to window is successful");
		   }
		  catch(Exception e){
		  
		   APP_LOGS.debug("exception caught :  "+e);
		    }
		    return Constants.KEYWORD_PASS;
		  
		  }
		  
		  public String waitTillNoOfWindows(final int numberOfWindows){
		   APP_LOGS.debug("Waiting till number of windows becomes 2");
		   try{
		     new WebDriverWait(driver,30){}.until(new ExpectedCondition<Boolean>()
		                      {
		             @Override public Boolean apply(WebDriver driver)
		                           {                       
		                 return (driver.getWindowHandles().size() == numberOfWindows);
		                           }
		                     });
		   }
		   catch(Exception e){
		    return Constants.KEYWORD_FAIL+"Number of windows is not equal to 2"+e.getMessage();
		   }
		   return Constants.KEYWORD_PASS;
		  }
	 
	 
	 public  String changeFocusDown(String object,String data){
		  APP_LOGS.debug("Changing the focus to top window ");
		  try{
			  driver.close();
		      driver.switchTo().window(Constants.OldTab);
		      
		  }
		  catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
		   }
		  return Constants.KEYWORD_PASS;
		}
	 
	public  String writeRandomFilename(String object,String data){
		APP_LOGS.debug("Writing in text box");
		try{
			String str1= data.split("cc")[0] + Integer.toString((int)(Math.random()*100000)) + data.split("cc")[1];
			driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(str1);
			Constants.Filename=str1;
		}
		catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}
	
	public  String writeRandomStringById(String object,String data){
		APP_LOGS.debug("Writing in text box");
		try{
			String str= data + Integer.toString((int)(Math.random()*10000));
			driver.findElement(By.id(OR	.getProperty(object))).sendKeys(str);
		}
		catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}

	
	public  String searchInMemberLookup(String object,String data){
		APP_LOGS.debug("Writing in text box");
		
		try{
			driver.findElement(By.id(OR	.getProperty(object))).sendKeys(Constants.EmailSignUpReferrer);
				
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}

	public  String DataToBeWritteninFile(File Fname,RandomAccessFile x, String p){
		APP_LOGS.debug("Writing in file");
		
		try{
			String path=p;
			RandomAccessFile raf=x;
			File file=Fname;
			
			String pattern = "dd/MM/yyyy:HH:mm";
	       SimpleDateFormat format = new SimpleDateFormat(pattern);
	       // formatting
	       String date=format.format(new Date());
	       
	     
	       long fileLength = file.length();
       	
	       raf.seek(0);
	       
       	raf.seek(fileLength+1);
    	
    	raf.writeBytes(System.getProperty("line.separator"));
    	raf.writeBytes(date);
    	raf.writeBytes("-----");
       
        if(path.contains("ValidReferrerarticfacts")||path.contains("InvalidReferrerarticfacts")||path.contains("UnknownReferrerarticfacts")||path.contains("ClosedReferrerarticfacts"))
        	 {
        	raf.writeBytes(Constants.EmailSignUpReferrer); 
            
        	 }
        else if(path.contains("Refereearticfacts"))
        {
        	raf.writeBytes(Constants.Referee); 
            
        }   	

        raf.close();
        
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
		}
		return Constants.KEYWORD_PASS;	
	}
	
	public String writeCSVData(String object,String data){
        APP_LOGS.debug("Write Data to a file or append data to a file");
        try{
        	String path = System.getProperty("user.dir")+"\\src\\ValidReferrerarticfacts.txt";
        	
        	if(Constants.ActualText_Expected.equalsIgnoreCase("Valid"))
        	{
        	File file = new File(path);
        	RandomAccessFile raf = new RandomAccessFile(file, "rw");
        		raf.seek(0);
        		if(raf.readLine()==null)
        		raf.writeBytes("  Time Stamp       -----   Valid Referrers");
        		else 
        			
        		DataToBeWritteninFile(file,raf, path);
        		
        		WriteInXLSFile(data);     	   	
        }
        
       else
    	   APP_LOGS.debug("Referrer's stats is not Valid hence not abble to add artifacts in file");
        
        }
        	catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Encountered unexpected error while writing data to file "+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS;
	}
	
public  String WriteInXLSFile(String data) throws IOException {
		APP_LOGS.debug("Writing in XLS file in a specified column");
		
		WebFileXLS= new Xls_Reader(System.getProperty("user.dir")
				+ data);
		int WebRowNum=WebFileXLS.getRowCount(Constants.TEST_CASES_SHEET);
		if(CurrentRowWeb<=WebRowNum)
		{		
		String TestCaseName=WebFileXLS.getCellData(Constants.TEST_CASES_SHEET, 0, CurrentRowWeb);		
		CurrentRowWeb++;
		WebFileXLS.setCellData(TestCaseName, "email", 2, Constants.EmailSignUpReferrer);
	
		}
		else
		{
							
		}
		return Constants.KEYWORD_PASS;
	}	
	
	
	
	public  String checkCheckBox(String object,String data){
		APP_LOGS.debug("Checking checkbox");
		try{
			// true or null
			String checked=driver.findElement(By.xpath(OR.getProperty(object))).getAttribute("checked");
			if(checked==null)// checkbox is unchecked
				driver.findElement(By.xpath(OR.getProperty(object))).click();
			   Thread.sleep(1000L);
		}catch(Exception e){
			return Constants.KEYWORD_FAIL+" - Could not find checkbox";
		}
		return Constants.KEYWORD_PASS;
	}	
	
	
	
//***********************************Click Link Types*******************************************************************//
	public String importFilenameIsEnabled(String object,String data){
        APP_LOGS.debug("Clicking on link by content");
        try{
        	if(driver.findElement(By.partialLinkText(Constants.Filename)).isEnabled() == false)
        	{
        	return Constants.KEYWORD_FAIL+" -- Not able to click on link ";
        	}
        	
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
    	return Constants.KEYWORD_PASS;
	}
	 
	 
	 public  String VerifyTextOfLastColumnOfLastRow(String object,String data){
        APP_LOGS.debug("Clicking on last column element of last row of a table");
        try{
        	  Thread.sleep(5000L);
        	  Properties properties=LoadProperties();
	           String MemberSiteURL =properties.getProperty("MemberSiteURL");
 //       	System.out.println("Expected status of the referral is    "+ data);
        	 waitTillVisibilityOfElementList(driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr")));
        	 waitTillVisibilityOfElementList(driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr/td[5]/a")));
         List<WebElement> TableRow=driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr"));
         
         
         int NumOfRows=TableRow.size();
         int LastRow=NumOfRows-1;
         List<WebElement> LastRowCol=TableRow.get(LastRow).findElements(By.tagName("td"));
        
         
         int NumOfCol=LastRowCol.size(); 
         int currentRow=100;
         List<WebElement> RefereeMailElement=driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr/td[5]/a"));
         int EmailListSize=RefereeMailElement.size()-1;
         
         while(EmailListSize>=0)
         { 
          String RefereeMail=RefereeMailElement.get(EmailListSize).getText();
 //         System.out.println("Constants.Referee ==="+ Constants.EmailSignup + "Referee email in control center is   "+RefereeMail );
          if((Constants.EmailSignup).equals(RefereeMail))
           {
            
           currentRow=EmailListSize;
           break;
           }
           
           else
            EmailListSize=EmailListSize-1;
                   
         }
         
         if(currentRow!=100)
         {  
            String Actual=TableRow.get(currentRow+1).findElements(By.tagName("td")).get(NumOfCol-1).getText();
            
 //           System.out.println("Actual status of the referral is:  "+Actual );
            if(Actual.equalsIgnoreCase(data))
            {
            	APP_LOGS.debug("Actual text is: " + Actual);
            	APP_LOGS.debug("Expected text is: " + data);
            	
          APP_LOGS.debug("Actual text is equal to expected text");
          
          if(Actual.trim().equalsIgnoreCase("Pending Denial"))
          {
      // 	  System.out.println(TableRow.get(currentRow+1).findElements(By.tagName("td")).get(NumOfCol-4).getText());
        	  
        	String LinkText=  TableRow.get(currentRow+1).findElements(By.tagName("td")).get(NumOfCol-4).getText();
        	clickWebElemetByLink_Text(object,LinkText);

        	  
          }
          return Constants.KEYWORD_PASS;

            }
         
         else
         {
          APP_LOGS.debug("Actual text is not equal to expected text."+"  Actual: "+Actual+"  \n Expected: "+data);
         
          return Constants.KEYWORD_FAIL+" Actual text is not equal to expected text";
          
         }
         }
         
          //   return Constants.KEYWORD_PASS;
        }catch(Exception e){
         return Constants.KEYWORD_FAIL+" unable to wait for frame"+e.getMessage();
        }
	return Constants.KEYWORD_FAIL+"  Referee email is not found in the referral list";
        
        

    }
	 

	public String clickWebElementById(String object,String data){
        APP_LOGS.debug("Clicking on link by id");
        try{
        driver.findElement(By.id(OR.getProperty(object))).click();
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS ;
	}
	
	public String clickWebElemetByXpath(String object,String data){
        APP_LOGS.debug("Clicking on link by xpath");
        try{
        driver.findElement(By.xpath(OR.getProperty(object))).click();
        }catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        }
     
		return Constants.KEYWORD_PASS;
	}
	
	
	
	 public String clickWebElemetByLink_TextNum(String object,String data){
	 
        APP_LOGS.debug("Clicking on link by link text");
        try{
        	String p[]=data.split("k");
        	
			driver.findElement(By.partialLinkText(p[0])).click();
        	}
        catch(Exception e){
			return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
        	}
     
		return Constants.KEYWORD_PASS;
	}
	 
	 public String clickWebElemetByLink_Text(String object,String data){
		  
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
	        	waitTillElementClickable(By.partialLinkText(data));

	   driver.findElement(By.partialLinkText(data)).click();
	         }
	        catch(Exception e){
	   return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	         }
	     
	  return Constants.KEYWORD_PASS;
	 }
	 
	 public String writeEmailForSearch(String object,String data){
		 
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.EmailSignUpReferrer);
	        	}
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to write "+e.getMessage();
	        	}
	     
			return Constants.KEYWORD_PASS;
		}

		  
		  public  String clickEmailAndchangeFocusNewWindowReferee(String object,String data){
			  APP_LOGS.debug("Changing the focus to top window ");
			  
			  try{
			   // considering that there is only one tab opened in that point.
			      String oldTab = driver.getWindowHandle();
			      
			      ArrayList<String> HandlesBeforeClick=new ArrayList<String>(driver.getWindowHandles());
			      
			      
			      int CurrentNoOfHandle=HandlesBeforeClick.size();
			      
			      driver.findElement(By.partialLinkText(Constants.EmailSignup)).click();
			      waitTillNoOfWindows(CurrentNoOfHandle+1);
			      ArrayList<String> HandlesAfterClick =new ArrayList<String>(driver.getWindowHandles());
		//	      System.out.println("Number of handles after click: "+HandlesAfterClick.size());
			      
			      
			     
			      if(HandlesAfterClick.contains(oldTab))
			    	  HandlesAfterClick.remove(oldTab);
					   
					   APP_LOGS.debug("no. of handles after removing parent handle"+ HandlesAfterClick);
					   for(String winHandle : HandlesAfterClick){  
					    APP_LOGS.debug("window handle after"+ winHandle );
					    driver.switchTo().window(winHandle);
					    APP_LOGS.debug("Second window handle="+ winHandle);
			      
			     
					   }
			  }
			  
		  catch(Exception e){
		   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
		   }
		  return Constants.KEYWORD_PASS;
	} 
		  
	 	   public  String OpenWebpageInNewTab(String object,String data){
			    APP_LOGS.debug("Opening webpage in new tab");
			    
			    try{	      
			     Robot robot= new Robot();
			     robot.keyPress(KeyEvent.VK_CONTROL);
			     robot.keyPress(KeyEvent.VK_T);
			     robot.keyRelease(KeyEvent.VK_T);
			     robot.keyRelease(KeyEvent.VK_CONTROL);			     
			     driver.get(data);			    
			    }catch(Exception e){
			     return Constants.KEYWORD_FAIL+" Unable to open page "+e.getMessage();
			    }
			    return Constants.KEYWORD_PASS;
			    
			   }
		  
//**********************************Keywords created*******************************************************
		      
		
		  
		  public  String waitTillPageRefreshed(String object,String data){
				APP_LOGS.debug("Waiting for page to refresh");
				try{
					ExpectedCondition<Boolean> expected=new ExpectedCondition<Boolean>(){
						@Override
		                public Boolean apply(WebDriver driver) {
							return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
						}
					};
					WebDriverWait wait=new WebDriverWait(driver,60);					
					wait.until(expected);
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to wait"+e.getMessage();
				}				
			}
		  
		  public  String waitTillObjecttextVisibility(String object,String data){
				APP_LOGS.debug("Waiting for an element to be visible");
				try{
					WebDriverWait wait=new WebDriverWait(driver,25);
					wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath(OR.getProperty(object)), data));
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to wait for element"+e.getMessage();
				}
			}
		  
		  public  String waitTillFrameVisibilityAndSwitchToIt(String object,String data){
				APP_LOGS.debug("Waiting for a frame to be visible");
				try{
					WebDriverWait wait=new WebDriverWait(driver,60);					
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(OR.getProperty(object)));
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- unable to wait for frame"+e.getMessage();
				}				
			}
		  public  String waitTillObjectVisibility(String object,String data){
				APP_LOGS.debug("Waiting for an element to be visible");
				try{
					WebDriverWait wait=new WebDriverWait(driver,60);					
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.getProperty(object))));
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to wait for element"+e.getMessage();
				}
			}
		  public  String SaveCurrentURLinGlobalVariable(String object,String data){
				APP_LOGS.debug("Saving current url in a global variable");
				try{
					CurrentURL=driver.getCurrentUrl();
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to open URL"+e.getMessage();
				}
			}
		  public  String OpenURLFromGlobalVariable(String object,String data){
				APP_LOGS.debug("Opening a URL saved in a global variable");
				try{
					driver.get(CurrentURL);
					return Constants.KEYWORD_PASS;
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+"unable to open URL"+e.getMessage();
				}
			} 
		  
		  public  String submitform(String object,String data){
	          APP_LOGS.debug("submitting form");
	          try{
	              driver.findElement(By.id(OR.getProperty(object))).submit();
	              }catch(Exception e){
	         return Constants.KEYWORD_FAIL+" -- Not able to click on Button"+e.getMessage();
	              }	          
	    return Constants.KEYWORD_PASS;
	   }
	   
	
		  public String clickMailTrapMail(String object,String data){
				 
		        APP_LOGS.debug("Clicking on mail in MailTrap");
		        try{
		        	int flag=0;
		        	long initialTime= System.currentTimeMillis();
		        	do{
		 List<WebElement> EmailList =driver.findElements(By.className("highlight"));	
		 
		 for(int i=0; i<EmailList.size();i++)
		 {
			 flag=0;
			 
			 if(EmailList.get(i).getText().equalsIgnoreCase(Constants.Referee))
			 {				 
				 EmailList.get(i).click();
				 break;
		     }
			 else
			  { 				 
				 flag=1;
			  }
		 }
		 if(flag==1)
			 
		
		 {
			Thread.sleep(5000L);
			driver.navigate().refresh();	
		Thread.sleep(2000L);
		
		 }
	
		}while(flag==1&&initialTime+60000L>System.currentTimeMillis());
		        	if(flag==1)
		        	    return Constants.KEYWORD_FAIL+ "The required email is not found ";
		        	}
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
		        	}
		     
				return Constants.KEYWORD_PASS;
			}
		  
		  public String clickMailTrapMailVerReferral(String object,String data){
				 
		        APP_LOGS.debug("Clicking on mail in MailTrap");
		        try{
		        	int flag=0;
		        	long initialTime= System.currentTimeMillis();
		        	do{
		 List<WebElement> EmailList =driver.findElements(By.className("highlight"));	
		 
		 for(int i=0; i<EmailList.size();i++)
		 {
			 flag=0;
			 
			 if(EmailList.get(i).getText().equalsIgnoreCase(data))
			 {				 
				 EmailList.get(i).click();
				 break;
		     }
			 else
			  { 				 
				 flag=1;
			  }
		 }
		 if(flag==1)
			 
		
		 {
			  
			Thread.sleep(5000L);
			driver.navigate().refresh();	
		Thread.sleep(2000L);
		
		 }
	
		}while(flag==1&&System.currentTimeMillis()-initialTime<60000L);
		 
		        	}
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
		        	}
		     
				return Constants.KEYWORD_PASS;
			}
		  
		  public String ClickElementByName(String object,String data){
		        APP_LOGS.debug("Clicking on element");
		        try{
		        	
		        	driver.findElement(By.name(data)).click();
		        }
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Unable to click"+e.getMessage();
		        }
		        return Constants.KEYWORD_PASS;
				
			}
		  public String selectFrameMailtrap(String object,String data){
		        APP_LOGS.debug("Select an Iframe with dynamically generated ID ");
		        try{
		        	String abc = driver.findElement(By.tagName("iframe")).getAttribute("src");
					driver.get(abc);
		        }
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to select frame "+e.getMessage();
		        }
		        return Constants.KEYWORD_PASS;
		  }
		    
		    public String clickMailTrapMailReward(String object,String data){
				 
		        APP_LOGS.debug("Clicking on mail in MailTrap");
		        try{
		        	int flag=0;
		        	long initialTime= System.currentTimeMillis();
		        	do{
		 List<WebElement> RewardList =driver.findElements(By.className("title"));
		 List<WebElement> EmailList =driver.findElements(By.className("highlight"));		 
		 for(int i=0; i<EmailList.size();i++)
		 {
			 flag=0;
			 
			 if(EmailList.get(i).getText().equalsIgnoreCase(Constants.Referee)&&RewardList.get(i).getText().contains("Congratulations"))
			 {
				 
				 EmailList.get(i).click();
				 break;
		     }
			 else
			  { 				 
				 flag=1;
			  }
		 }
		 if(flag==1)		
		 {
			Thread.sleep(5000L);
			driver.navigate().refresh();
	
		Thread.sleep(2000L);
		
		 }
	
		}while(flag==1&&initialTime+60000L>System.currentTimeMillis());
		 
		        	}
		        catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to click on mail "+e.getMessage();
		        	}
		     
				return Constants.KEYWORD_PASS;
			}
		    
		   
		    public String ClickLinkByVerifyingTextInNextCol(String object,String data){		
				APP_LOGS.debug("Clicking on link");
				try{
					String WaitElementsResult1=waitTillVisibilityOfElementList(driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr/td")));
					String WaitElementsResult2=waitTillVisibilityOfElementList(driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr/td/a")));
					APP_LOGS.debug(WaitElementsResult1);
					APP_LOGS.debug(WaitElementsResult2);
						 List<WebElement> TableColProcessDetail=driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr/td"));
						 List<WebElement> TableColProcessId=driver.findElements(By.xpath("//*[@class='record_list']/tbody/tr/td/a"));
						 int NumOfCol=TableColProcessDetail.size();
						 String MacthCol="defaultText";
						 for(int j=0; j<NumOfCol; j++)
						 {				 
						 if(TableColProcessDetail.get(j).getText().trim().equalsIgnoreCase(data))
						 MacthCol=TableColProcessDetail.get(j-1).getText();					
						 }
						   for(int i=0; i<TableColProcessId.size(); i++)
						   {						 
						     if(TableColProcessId.get(i).getText().trim().equalsIgnoreCase(MacthCol))
						     {
						    	 TableColProcessId.get(i).click();
							 break;
						     }
						  }
					 
				}catch(Exception e){
					return Constants.KEYWORD_FAIL+" -- Not able to click";
				}
				return Constants.KEYWORD_PASS;
			}
				  
				
				

				public  String SetQATimeGeneralized(String object,String data){
				    APP_LOGS.debug("Setting QA time of the desired site");
				  
				    try{			      
				    	Properties properties=LoadProperties();
				     String MemberSiteURL=properties.getProperty("MemberSiteURL");
				     
				   String QATimeURL=MemberSiteURL+"/qaTime.jsp?action=set&time=";
				    
				     String pattern = "yyyy/MM/dd:HH:mm";
				           SimpleDateFormat format = new SimpleDateFormat(pattern);
				           // formatting
				           String date=format.format(new Date());
				           Calendar cal = Calendar.getInstance();
				          
				           int DaysToBeAdded = Integer.valueOf(OR.getProperty(object))+Integer.valueOf(data);
				           
				            cal.add(Calendar.DATE, DaysToBeAdded);
				          
				            Date NewDate = cal.getTime();    
				            String ModifiedDate = format.format(NewDate);    
				     driver.get(QATimeURL+ModifiedDate);
				     
				    }catch(Exception e){
				     return Constants.KEYWORD_FAIL+" Unable to open page "+e.getMessage();

				    }
				    return Constants.KEYWORD_PASS;
				    
				   }

				 
				 public  String writeFileNameInInputByXpath(String object,String data){
					    APP_LOGS.debug("Writing in text box");
					    
					    try{
					     driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.ReferrerImportCSV);
					    }catch(Exception e){
					     return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

					    }
					    return Constants.KEYWORD_PASS;
					    
					   }
				 
				 public String ClickclientnameByName(String object,String data){
				        APP_LOGS.debug("Clicking on element");
				        try{
				        	
				        	driver.findElement(By.name(OR.getProperty(object))).click();
				        }
				        catch(Exception e){
							return Constants.KEYWORD_FAIL+" -- Unable to click"+e.getMessage();
				        }
				        return Constants.KEYWORD_PASS;
						
					}

				 
				 public String clickMailTrapMailRewardReferrer(String object,String data){
				     
			          APP_LOGS.debug("Clicking on mail in MailTrap");
			          try{
			           int flag=0;
			           long initialTime= System.currentTimeMillis();
			           do{
			   List<WebElement> RewardList =driver.findElements(By.className("title"));
			   List<WebElement> EmailList =driver.findElements(By.className("highlight"));
	
			   
			   for(int i=0; i<EmailList.size();i++)
			   {
			    flag=0;
			    
			    if(EmailList.get(i).getText().equalsIgnoreCase(data)&&RewardList.get(i).getText().contains("Congratulations"))
			    {
			     
			     EmailList.get(i).click();
			     break;
			       }
			    else
			     { 
			     
			     flag=1;
			     }
			    if(EmailList.get(i).getText().equalsIgnoreCase(data)&&RewardList.get(i).getText().contains("Status Notification"))
			    {
			     
			     EmailList.get(i).click();
			     flag=0;
			     break;
			       }
			    else
			     { 
			     
			     flag=1;
			     }
			   }
			   if(flag==1)			  
			   {
			     Thread.sleep(5000L);
			   driver.navigate().refresh();
			 
			  Thread.sleep(2000L);
			  
			   }
			 
			  }while(flag==1&&initialTime+60000L>System.currentTimeMillis());
			           if(flag==1)
			               return Constants.KEYWORD_FAIL+ "Unable to click ";
			           }
			          catch(Exception e){
			     return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
			           }
			       
			    return Constants.KEYWORD_PASS;
			   }
			    public String VerifyRewardQuantityReferee(String object,String data){  
			     APP_LOGS.debug("Verifying the quantity");
			     try{
			      
			    	 Properties properties=LoadProperties();
			               String RewardQtyExpected_Referee=properties.getProperty("RefereeRewardAmount");
			                              
			    WebElement Element=driver.findElement(By.xpath(OR.getProperty(object)));
			    
			    String Reward=Element.getText();
			    String[] RewardSplit=Reward.split(":");
			    int RewardQty=Integer.valueOf(RewardSplit[1].trim());
			    if(Integer.valueOf(RewardQtyExpected_Referee)==RewardQty)
			    {
			    	APP_LOGS.debug("reward quantity matches");
			     
			    }
			    else
			    	APP_LOGS.debug("reward quantity does not matches");
			    
			     }catch(Exception e){
			      return Constants.KEYWORD_FAIL+" -- Not able to find element ";
			     }
			     return Constants.KEYWORD_PASS;
			    }
			    
			    public String VerifyRewardQuantityReferrer(String object,String data){  
			        APP_LOGS.debug("Verifying the quantity");
			        try{			         
			        	Properties properties=LoadProperties();
			                  String RewardQtyExpected_Referrer=properties.getProperty("ReferrerRewardAmount");
			                  
			       WebElement Element=driver.findElement(By.xpath(OR.getProperty(object)));
			       
			       String Reward=Element.getText();
			       String[] RewardSplit=Reward.split(":");
			       int RewardQty=Integer.valueOf(RewardSplit[1].trim());
			       if(Integer.valueOf(RewardQtyExpected_Referrer)==RewardQty)
			       {
			    		        
			       }
			       else
			    	   APP_LOGS.debug("reward quantity does not matches");
			       
			        }catch(Exception e){
			         return Constants.KEYWORD_FAIL+" -- Not able to find element ";
			        }
			        return Constants.KEYWORD_PASS;
			       }
			    public String clickMailTrapMailRewardReferee(String object,String data){
			        
			          APP_LOGS.debug("Clicking on mail in MailTrap");
			          try{
			           int flag=0;
			           long initialTime= System.currentTimeMillis();
			           do{
			   List<WebElement> RewardList =driver.findElements(By.className("title"));
			   List<WebElement> EmailList =driver.findElements(By.className("highlight"));
			   for(int i=0; i<EmailList.size();i++)
			   {
			    flag=0;
			    
			    if(EmailList.get(i).getText().equalsIgnoreCase(Constants.Referee)&&RewardList.get(i).getText().contains("Congratulations"))
			    {
			     
			     EmailList.get(i).click();
			     break;
			       }
			    else
			     { 
			     
			     flag=1;
			     }
			    if(EmailList.get(i).getText().equalsIgnoreCase(Constants.Referee)&&RewardList.get(i).getText().contains("Status Notification"))
			    {
			     
			     EmailList.get(i).click();
			     flag=0;
			     break;
			       }
			    else
			     { 
			     flag=1;
			     }
			   }
			   if(flag==1)			  
			   {
			    Thread.sleep(5000L);
			   driver.navigate().refresh();
			 
			  Thread.sleep(2000L);
			  
			   }
			 
			  }while(flag==1&&initialTime+60000L>System.currentTimeMillis());
			           if(flag==1)
			               return Constants.KEYWORD_FAIL+ "The required email is not found ";
			           }
			          catch(Exception e){
			     return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
			           }
			       
			    return Constants.KEYWORD_PASS;
			   }
			    
			    
			    public String writeCSVDataforClosedreferrer(String object,String data){
			        APP_LOGS.debug("Write Data to a file or append data to a file");
			        try{
			        	String path = System.getProperty("user.dir")+"\\src\\ClosedReferrerarticfacts.txt";
	//		        	System.out.println("Value stored in Constants.ActualText_Expected ; "+Constants.ActualText_Expected);
			        	if(Constants.ActualText_Expected.equalsIgnoreCase("Closed"))
			        	{
			        	File file = new File(path);
			        	RandomAccessFile raf = new RandomAccessFile(file, "rw");
			        		raf.seek(0);
			        		if(raf.readLine()==null)
			        		raf.writeBytes("  Time Stamp       -----   Closed Referrers");
			        		else 
			        			
			        		DataToBeWritteninFile(file,raf, path);
			        		
			        		WriteInXLSFileforClosedreferrer(data);     	   	
			        }
			        			       else
			        			       {
			        			    	   APP_LOGS.debug("Referrer's stats is not Closed hence not able to add artifacts in file");
			        			    	   return Constants.KEYWORD_FAIL;
			        			       }
			        
			        }
			        	catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Encountered unexpected error while writing data to file "+e.getMessage();
			        }
			     
					return Constants.KEYWORD_PASS;
				}
				
			    public  String WriteInXLSFileforClosedreferrer(String data) throws IOException {
					APP_LOGS.debug("Writing in XLS file in a specified column");
					
					WebFileXLS= new Xls_Reader(System.getProperty("user.dir")
							+ data);
					
				WebFileXLS.setCellData("ReferrerCloseAcctReferralDenied", "email", 2, Constants.EmailSignUpReferrer);
				
					
					return Constants.KEYWORD_PASS;
				}	
			    public void captureScreenShot(String Filename) throws IOException {
					APP_LOGS.debug("taking screen shot according to config");
				    	  
				  try{
						File scrFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
						FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+"//Screenshots//"+Filename+".jpg"));
					 }
				  catch(Exception e)
				  {
					  APP_LOGS.debug("not able to take screen shots"+e.getMessage());
				  }
				}
			    
			    
			       public String VerifyAccntStatus(String object,String data) {  
			          APP_LOGS.debug("Clicking on link");
			          try{
			           
			              waitTillVisibilityOfAllElements("//*[@class='pageBodyTable']/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr/td[3]",data);

			              int flag=0;
			             List<WebElement> TableFieldDesc=driver.findElements(By.xpath("//*[@class='pageBodyTable']/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr/td[3]"));
			             int NumOfCol=TableFieldDesc.size();
			             for(int j=0; j<NumOfCol; j++)
			             {  
			             if(TableFieldDesc.get(j).getText().trim().equalsIgnoreCase(data))
			             { 
			             flag=1;
			             Constants.ActualText_Expected=TableFieldDesc.get(j).getText().trim();
			             break;
			             }
			             else
			              Constants.ActualText_NotExpected=TableFieldDesc.get(j).getText().trim();
			              
			             }
			              
			             if(flag==1)
			            return Constants.KEYWORD_PASS;
			             else
			             return Constants.KEYWORD_FAIL+"Account status is not as expected";

			          }catch(Exception e){
			           return Constants.KEYWORD_FAIL+" -- Not able to click";
			          }
			         
			         }
			       
			    public String checkIfWebElementEnabled(String object,String data){
			           APP_LOGS.debug("Clicking on link by content");
			           try{
			        	   
			             int flag=0;
			             Thread.sleep(5000L);
			             List<WebElement> TableFieldDesc=driver.findElements(By.xpath("//*[@class='pageBodyTable']/tbody/tr[3]/td/table[1]/tbody/tr/td/table/tbody/tr/td[1]"));
			            List<WebElement> TableFieldVal=driver.findElements(By.xpath("//*[@class='pageBodyTable']/tbody/tr[3]/td/table[1]/tbody/tr/td/table/tbody/tr/td[3]"));
			           
			            int NumOfRowsDesc=TableFieldDesc.size();  
			        
			            for(int j=4; j<NumOfRowsDesc; j++)
			            {    
			            if(TableFieldDesc.get(j).getText().trim().equalsIgnoreCase(data))
			            if(TableFieldVal.get(j-4).getText().trim().equalsIgnoreCase(Constants.MatchKey)||TableFieldVal.get(j-4).getText().trim().equalsIgnoreCase(Constants.RefereePhoneNumber)||TableFieldVal.get(j-4).getText().trim().equalsIgnoreCase(Constants.Referee))
			             { if(TableFieldVal.get(j-4).isEnabled())
			            	flag=1;
			             break;
			             }
			            
			           }
			            if(flag==1)
			           return Constants.KEYWORD_PASS;
			            else
			            	return Constants.KEYWORD_FAIL+"Account number is not enabled";

			           }
			            catch(Exception e){
			      return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
			           }
			        
			    }
	 public String VerifyDenialReason(String object,String data){
	           APP_LOGS.debug("Clicking on link by content");
	           try{
	        	   
	             int flag=0;
	             Thread.sleep(5000L);
	             List<WebElement> TableFieldDesc=driver.findElements(By.xpath("//*[@class='pageBodyTable']/tbody/tr[3]/td/table[1]/tbody/tr/td[1]"));
	            List<WebElement> TableFieldVal=driver.findElements(By.xpath("//*[@class='pageBodyTable']/tbody/tr[3]/td/table[1]/tbody/tr/td[3]"));
	           
	            int NumOfRowsDesc=TableFieldDesc.size();

	            for(int j=1; j<NumOfRowsDesc; j++)
	            {  
	          //  	System.out.println("Text to be verified:  "+ TableFieldDesc.get(j).getText());
	            if(TableFieldDesc.get(j).getText().trim().equalsIgnoreCase("Denial Reason:"))
	            	
	            {
	            if(TableFieldVal.get(j).getText().trim().equalsIgnoreCase(data));
	            {
	           // 	System.out.println("String is: "+TableFieldVal.get(j).getText() );
	            return (Constants.KEYWORD_PASS + "  The Denial Reason is as expected");
	            }
	            }
	           }
	            
	            
	            	return (Constants.KEYWORD_FAIL+"  Denial Reason is not displayed");

	           }
	            catch(Exception e){
	      return Constants.KEYWORD_FAIL+" -- Not able to verify Denial Reason "+e.getMessage();
	           }
	        
	    }
       public String checkIfWebElementEnabledReferrer(String object,String data){
			           APP_LOGS.debug("Clicking on link by content");
			           try{

				             int flag=0;
				             Thread.sleep(5000L);
				             List<WebElement> TableFieldDesc=driver.findElements(By.xpath("//*[@class='pageBodyTable']/tbody/tr[3]/td/table[1]/tbody/tr/td/table/tbody/tr/td[1]"));
				            List<WebElement> TableFieldVal=driver.findElements(By.xpath("//*[@class='pageBodyTable']/tbody/tr[3]/td/table[1]/tbody/tr/td/table/tbody/tr/td[3]"));

				            int NumOfRowsDesc=TableFieldDesc.size();

				            for(int j=4; j<NumOfRowsDesc; j++)
				            {  
				            if(TableFieldDesc.get(j).getText().trim().equalsIgnoreCase(data))
				            { if(TableFieldVal.get(j-4).getText().trim().equalsIgnoreCase(Constants.MatchKey)||TableFieldVal.get(j-4).getText().trim().equalsIgnoreCase(Constants.RefereePhoneNumber)||TableFieldVal.get(j-4).getText().trim().equalsIgnoreCase(Constants.Referrer))
				             { if(TableFieldVal.get(j-4).isEnabled())
				            	flag=1;
				             break;
				             }
				            }
				           }
				            if(flag==1)
				           return Constants.KEYWORD_PASS;
				            else
				            	return Constants.KEYWORD_FAIL+"Account number is not enabled";
	                       }
			            catch(Exception e){
			      return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
			           }
			        
			    }

			    public String SwitchToNewWindowAndClosePrevious(String object, String data){
					APP_LOGS.debug("Switching to new window");
					try{ 
				String Parenthandle=driver.getWindowHandle();
				APP_LOGS.debug("ParentWindowHandle is" + Parenthandle );
				APP_LOGS.debug("Driver is in the following url" +driver.getCurrentUrl());
				Set<String> HandlesBeforeClick=driver.getWindowHandles();
				int CurrentNoOfHandle=HandlesBeforeClick.size();
					driver.findElement(By.xpath(OR.getProperty(object))).click();
					APP_LOGS.debug("Button is clicked");
					
					waitTillNoOfWindows(CurrentNoOfHandle+1);
					driver.close();
					Set<String> handle=driver.getWindowHandles();
					APP_LOGS.debug("total number of handles= "+ handle);					
					APP_LOGS.debug("no. of handles after removing parent handle"+ handle);
					for(String winHandle : handle){		
						APP_LOGS.debug("window handle after"+ winHandle );
						driver.switchTo().window(winHandle);
						APP_LOGS.debug("Second window handle="+ winHandle);					
				}
			    APP_LOGS.debug("Switch to window is successful");
					}
				catch(Exception e){
				
					APP_LOGS.debug("exception caught :  "+e);
						}
						return Constants.KEYWORD_PASS;			
				}
			    
			    public String writeCSVDataForInvalidReferrer(String object,String data){
			        APP_LOGS.debug("Write Data to a file or append data to a file");
			        try{
			        	String path = System.getProperty("user.dir")+"\\src\\InvalidReferrerarticfacts.txt";
			        	
			        	if(Constants.ActualText_Expected.equalsIgnoreCase("InValid"))
			        	{
			        	File file = new File(path);
			        	RandomAccessFile raf = new RandomAccessFile(file, "rw");
			        		raf.seek(0);
			        		if(raf.readLine()==null)
			        		raf.writeBytes("  Time Stamp       -----   InValid Referrers");
			        		else 
			        			
			        		DataToBeWritteninFile(file,raf, path);
			        		
			        		WriteInXLSFileforInvalidreferrer(data);     	   	
			        }
			        
			        		
			       else
			    	   APP_LOGS.debug("Referrer's stats is not Valid hence not abble to add artifacts in file");
			        
			        }
			        	catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Encountered unexpected error while writing data to file "+e.getMessage();
			        }
			     
					return Constants.KEYWORD_PASS;
				}
			    
			    public  String WriteInXLSFileforInvalidreferrer(String data) throws IOException {
					APP_LOGS.debug("Writing in XLS file in a specified column");
					
					WebFileXLS= new Xls_Reader(System.getProperty("user.dir")
							+ data);
					
				WebFileXLS.setCellData("ReferrerInvalid", "emailinvalid", 2, Constants.EmailSignUpReferrer);
				
					
					return Constants.KEYWORD_PASS;
				}
			    public String writeCSVDataForReferrerDontEnterAccntno(String object,String data){
			        APP_LOGS.debug("Write Data to a file or append data to a file");
			        try{
			        	String path = System.getProperty("user.dir")+"\\src\\UnknownReferrerarticfacts.txt";
			        	
			        	if(Constants.ActualText_Expected.equalsIgnoreCase("Unknown"))
			        	{
			        	File file = new File(path);
			        	RandomAccessFile raf = new RandomAccessFile(file, "rw");
			        		raf.seek(0);
			        		if(raf.readLine()==null)
			        		raf.writeBytes("  Time Stamp       -----   Unknown Referrers");
			        		else 
			        			
			        		DataToBeWritteninFile(file,raf, path);
			        		
			        		WriteInXLSFileforReferrerDontEnterAccntno(data);     	   	
			        }
			        
			       else
			    	   APP_LOGS.debug("Referrer's stats is not Valid hence not abble to add artifacts in file");
			        
			        }
			        	catch(Exception e){
						return Constants.KEYWORD_FAIL+" -- Encountered unexpected error while writing data to file "+e.getMessage();
			        }
			     
					return Constants.KEYWORD_PASS;
				}
			    
			    public  String WriteInXLSFileforReferrerDontEnterAccntno(String data) throws IOException {
					APP_LOGS.debug("Writing in XLS file in a specified column");
					
					WebFileXLS= new Xls_Reader(System.getProperty("user.dir")
							+ data);
					
					WebFileXLS.setCellData("ReferrerdoesntEnterAccntNum", "emailinvalid", 2, Constants.EmailSignUpReferrer);
				
					
					return Constants.KEYWORD_PASS;
				}			  
			    
			    public boolean IsElementPresent(By by) {
			        try {
			          driver.findElement(by);
			          return true;
			        } catch (org.openqa.selenium.NoSuchElementException e) {
			          return false;
			        }
			    }
			    
			    public Properties LoadProperties(){
			        String path = System.getProperty("user.dir") + "\\src\\com\\PendingDenial\\config\\or.properties";
			            FileReader reader;
			           Properties properties = new Properties();
			      try {
			          reader = new FileReader(path);
			             properties.load(reader);
			              }catch (Exception e) {
			                e.printStackTrace();
			                 }
			      return properties;
			       }		  
			      
			    public  String WriteAccountInformation(String object, String data){
			          APP_LOGS.debug("Writing account information to different fields and saving them to a constant");
			          try{
			        	  Thread.sleep(2000L);
			           //String str;
			           Properties properties=LoadProperties();
			           String EmailUsedAsAccountNumber =properties.getProperty("EmailUsedAsAccountNumber");
			      String AccountNumberType =properties.getProperty("AccountNumberType(MatchKeyOrPhoneNumber)");
			      if (AccountNumberType.equalsIgnoreCase("Matchkey"))
			           
			      {
			          String MatchKeyStr= WriteMatchkey(data);
			           Constants.MatchKey=MatchKeyStr;
			      }
			      else if(AccountNumberType.equalsIgnoreCase("PhoneNumber"))
		              {
			              Constants.RefereePhoneNumber=String.valueOf((long)(Math.random()*100000 + 3333300000L) );
			              driver.findElement(By.id(OR.getProperty(object))).clear();
			              driver.findElement(By.id(OR.getProperty(object))).sendKeys(Constants.RefereePhoneNumber);
			          }
			      else if (EmailUsedAsAccountNumber.equalsIgnoreCase("yes"))
			      {
			    	  driver.findElement(By.id(OR.getProperty(object))).clear();
			    	  Constants.Referee="test"+Integer.toString((int)(Math.random()*10000))+"@test"+Integer.toString((int)(Math.random()*10000))+".com";
			    	  driver.findElement(By.id(OR.getProperty(object))).sendKeys(Constants.Referee);
			      }
			            
			      }    catch(Exception e){
			            
			     return Constants.KEYWORD_FAIL + " Unable to write " + e.getMessage();
			    
			               }
			           return Constants.KEYWORD_PASS;  
		}
		 
			      public  String waitTillObjectVisibilityID(String object,String data){
			          APP_LOGS.debug("Waiting for an element to be visible");
			          try{
			           WebDriverWait wait=new WebDriverWait(driver,60);
			           
			           wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(OR.getProperty(object))));
			           return Constants.KEYWORD_PASS;
			          }catch(Exception e){
			           return Constants.KEYWORD_FAIL+" -- unable to wait for element"+e.getMessage();
			          }
			         }
			      
			      public String clickReferrerEmailLinkAndChangeFocus(String object, String data){
			          APP_LOGS.debug("Switching to new window");
			          try{ 
			          waitTillElementClickable(By.partialLinkText(data));
			         String Parenthandle=driver.getWindowHandle();
			         Set<String> HandlesBeforeClick=driver.getWindowHandles();
			         int CurrentNoOfHandle=HandlesBeforeClick.size();
			          driver.findElement(By.partialLinkText(data)).click();
			          waitTillNoOfWindows(CurrentNoOfHandle+1);
			          driver.close();
			          Set<String> handle=driver.getWindowHandles();
			          for(String winHandle : handle){  
			          driver.switchTo().window(winHandle);
			          
			         }
			          }
			         catch(Exception e){
			         
			          APP_LOGS.debug("exception caught :  "+e);
			           }
			           return Constants.KEYWORD_PASS;
			        
			         }
			        public  String changeFocusToNewWindow(String object,String data){
			        APP_LOGS.debug("Changing the focus to top window ");
			        
			        try{
			         waitTilltextPartialLinkClickable(object,Constants.EmailSignup);
			         String Parenthandle=driver.getWindowHandle();
			       Set<String> HandlesBeforeClick=driver.getWindowHandles();
			        int CurrentNoOfHandle=HandlesBeforeClick.size();
			        driver.findElement(By.partialLinkText(Constants.EmailSignup)).click();
			        waitTillNoOfWindows(CurrentNoOfHandle+1);
			        Set<String> handle=driver.getWindowHandles();
			        for(String winHandle : handle){  
			          
			          driver.switchTo().window(winHandle);
			        }
			           APP_LOGS.debug("Switch to window is successful");
			        
			        }
			        catch(Exception e){
			         return Constants.KEYWORD_FAIL+" -- Unable to changefocus - "+e.getMessage();
			         }
			        return Constants.KEYWORD_PASS;
			     }
			        public  String changeFocusToNewWindowReferrer(String object,String data){
				        APP_LOGS.debug("Changing the focus to top window ");
				        
				        try{
	//			        	System.out.println("Referrer email address is: "+ data);
				         waitTilltextPartialLinkClickable(object,data);
				         String Parenthandle=driver.getWindowHandle();
				       Set<String> HandlesBeforeClick=driver.getWindowHandles();
				        int CurrentNoOfHandle=HandlesBeforeClick.size();
				        driver.findElement(By.partialLinkText(data)).click();
				        waitTillNoOfWindows(CurrentNoOfHandle+1);				  
				         Set<String> handle=driver.getWindowHandles();
				        for(String winHandle : handle){  				          
				          driver.switchTo().window(winHandle);
				        }
				           APP_LOGS.debug("Switch to window is successful");
				        
				        }
				        catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- Unable to changefocus - "+e.getMessage();
				         }
				        return Constants.KEYWORD_PASS;
				     }
			      
			       
			        public  String waitTilltextPartialLinkClickable(String object,String data){
				        APP_LOGS.debug("Waiting for an element to be visible");
				        try{
				         WebDriverWait wait=new WebDriverWait(driver,10);
				         wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(data)));
				         return Constants.KEYWORD_PASS;
				        }catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- unable to wait for element"+e.getMessage();
				        }
				       }
				        
				        public  String waitTillPresenceOfAllElements(String object,String data){
				        APP_LOGS.debug("Waiting for an element to be visible");
				        try{
				         WebDriverWait wait=new WebDriverWait(driver,10);
				         wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(object)));
				         return Constants.KEYWORD_PASS;
				        }catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- unable to wait for element"+e.getMessage();
				        }
				       }
				        
				        public  String waitTillVisibilityOfAllElements(String object,String data){
				        APP_LOGS.debug("Waiting for all elements to be visible");
				        try{
				         WebDriverWait wait=new WebDriverWait(driver,10);
				         wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(object)));
				         return Constants.KEYWORD_PASS;
				        }catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- unable to wait for elements"+e.getMessage();
				        }
				       }
				        
				        public  String waitTillVisibilityOfElementList(List<WebElement> ElementList){
				        APP_LOGS.debug("Waiting for all elements to be visible");
				        try{
				         WebDriverWait wait=new WebDriverWait(driver,10);
				         wait.until(ExpectedConditions.visibilityOfAllElements(ElementList));
				         return Constants.KEYWORD_PASS;
				        }catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- unable to wait for elements"+e.getMessage();
				        }
				       }
				        public  String waitTillElementClickable(By by){
				        APP_LOGS.debug("Waiting for button to be clickable");
				        try{
				         WebDriverWait wait=new WebDriverWait(driver,10);
				         wait.until(ExpectedConditions.elementToBeClickable(by));
				         return Constants.KEYWORD_PASS;
				        }catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- unable to wait for elements"+e.getMessage();
				        }
				       }
				        public  String waitTillElementClickableXpath(String object, String data){
				        APP_LOGS.debug("Waiting for element to be clickable");
				        try{
				         WebDriverWait wait=new WebDriverWait(driver,10);
				         wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty(object))));
				         return Constants.KEYWORD_PASS;
				        }catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- unable to wait for elements"+e.getMessage();
				        }
				       }
				        
				        public  String waitTillElementClickableID(String object, String data){
				        APP_LOGS.debug("Waiting for element to be clickable");
				        try{
				         WebDriverWait wait=new WebDriverWait(driver,10);
				         wait.until(ExpectedConditions.elementToBeClickable(By.id(OR.getProperty(object))));
				         return Constants.KEYWORD_PASS;
				        }catch(Exception e){
				         return Constants.KEYWORD_FAIL+" -- unable to wait for elements"+e.getMessage();
				        }
				       }
				        
				
				    public String selectFrameByName(String object,String data){
				           APP_LOGS.debug("Select an Iframe with dynamically generated ID ");
				           try{
				            String result= waitTillFrameVisibilityAndSwitchToIt(data);
				           APP_LOGS.debug(result);				            
				           }
				           catch(Exception e){
				      return Constants.KEYWORD_FAIL+" -- Not able to select frame "+e.getMessage();
				           }
				           return Constants.KEYWORD_PASS;
				     
				    }
public String clickEmailAndchangeFocusNewWindow(String object,String data){
		 
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
	        	String WaitElementResult=waitTilltextPartialLinkClickable(object,Constants.EmailSignUpReferrer);
	        	APP_LOGS.debug(WaitElementResult);
	        	// considering that there is only one tab opened in that point.
			      Set<String> oldTab = driver.getWindowHandles();
			      int CurrentNoOfHandle=oldTab.size();
			      driver.findElement(By.partialLinkText(Constants.EmailSignUpReferrer)).click();
					waitTillNoOfWindows(CurrentNoOfHandle+1);
			      ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
			      if(newTab.contains(oldTab))
			    	  newTab.remove(oldTab);						
						APP_LOGS.debug("no. of handles after removing parent handle"+ newTab);
						for(String winHandle : newTab){		
							APP_LOGS.debug("window handle after"+ winHandle );
	        	}
	        }
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        	}
	     
			return Constants.KEYWORD_PASS;
		}

				  public  String waitTillFrameVisibilityAndSwitchToIt(String FrameName){

					  APP_LOGS.debug("Waiting for a frame to be visible");
				      try{
				       WebDriverWait wait=new WebDriverWait(driver,10);				       
				       wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(FrameName));
			//	       System.out.println("Switched to frame-"+FrameName);
				       return Constants.KEYWORD_PASS+" -- Waited for the frame to be visible and successfully switched to it";
				      }catch(Exception e){
				       return Constants.KEYWORD_FAIL+" -- unable to wait for frame"+e.getMessage();
				      }
				      
				      
				     }

				  
				  public String clickCheckboxes(String object,String data){
		              APP_LOGS.debug("Clicking on all the checkboxes");
		              try{
		        //       System.out.println("Elements presents: "+(AreElementsPresent(By.xpath("//*[@type='checkbox']"))==true));
		               if((AreElementsPresent(By.xpath("//*[@type='checkbox']"))==true))
		                if(!driver.findElements(By.xpath("//*[@type='checkbox']")).isEmpty())
		               {	               
		              List<WebElement> Checkboxes=driver.findElements(By.xpath("//*[@type='checkbox']"));
		              String WaitResult=waitTillVisibilityOfElementList(Checkboxes);
		              APP_LOGS.debug(WaitResult);
		              int size= Checkboxes.size();
		              for(int i=0; i<size; i++)
		              {
		               Checkboxes.get(i).click();
		              }
		               }
		                else if ((AreElementsPresent(By.xpath("//*[@type='checkbox']"))==false))
		                 return Constants.KEYWORD_PASS;  
		                   
		              }catch(Exception e){
		         return Constants.KEYWORD_FAIL+" -- Not able to click on checkboxes "+e.getMessage();
		              }
		           
		        return Constants.KEYWORD_PASS;
		       }
				  
				  public  String closeWindow(String object, String data){
						APP_LOGS.debug("Closing the window");
						try{
							driver.close();
						}catch(Exception e){
							return Constants.KEYWORD_FAIL+"Unable to close window."+e.getMessage();
						}
						return Constants.KEYWORD_PASS;

					}
				  public  String SaveWindowHandleToConstant(String object,String data){
					  APP_LOGS.debug("Changing the focus to top window ");
					  try{
						  String Handle= driver.getWindowHandle();
					      Constants.WindowHandle=Handle;					      
					  }
					  catch(Exception e){
					   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
					   }
					  return Constants.KEYWORD_PASS;
					}
			    public  String SwitchToWinHandleSavedInConst(String object,String data){
					  APP_LOGS.debug("Changing the focus to top window ");
					  try{
						  
					      driver.switchTo().window(Constants.WindowHandle);
					      
					  }
					  catch(Exception e){
					   return Constants.KEYWORD_FAIL+" Unable to changefocus - "+e.getMessage();
					   }
					  return Constants.KEYWORD_PASS;
					}
			    
			    public String writePhoneNum()
			      {   int PhoneNumFlag=0;
			       if(IsElementPresent(By.name("521000000848#client.accnt.number"))==false)
			            APP_LOGS.debug("Phone number field is not present");        
			       else if(!driver.findElement(By.name("521000000848#client.accnt.number")).isDisplayed())
			        PhoneNumFlag=1;  
			       if(IsElementPresent(By.name("521000000848#client.accnt.number"))==true)
			        if(driver.findElement(By.name("521000000848#client.accnt.number")).isDisplayed())
			         {  
			         APP_LOGS.debug("Phone number field is displayed");
			         String RandomPhoneNum=String.valueOf((long)(Math.random()*100000 + 3333300000L) );
			         driver.findElement(By.name("521000000848#client.accnt.number")).clear();
			         driver.findElement(By.name("521000000848#client.accnt.number")).sendKeys(RandomPhoneNum);
			         Constants.RefereePhoneNumber = RandomPhoneNum;
			         Constants.PhoneNumFilled="Y";
			         }
			        if(PhoneNumFlag==1)
			         APP_LOGS.debug("Phone number field is not displayed");
			         return Constants.KEYWORD_PASS;    
			           
			      }
			      
	
			      public  String WriteAccntNumAtSignUp(String object,String data){
				        APP_LOGS.debug("Writing Account Number");
				        
				        try{
				         int flag=0;
				          String strPIN;
				         String strDate;
				        Properties properties =LoadProperties();
				                 String MatchKeyName_3=properties.getProperty("MatchKeyName_3");
				             Constants.AccountNoReferrer=String.valueOf((int)(1000+ Math.random()*8999));
				            Constants.Pin=String.valueOf((int)(10000+ Math.random()*89999));   
				           Constants.date=randomDataOfBirth(1914, 2010);
				          strPIN = data+"|"+Constants.AccountNoReferrer+"|"+Constants.Pin;
				          strDate = data+"|"+Constants.AccountNoReferrer+"|"+Constants.date;
				           if(IsElementPresent(By.name(OR.getProperty(object)))==false)
				              if((IsElementPresent(By.id("reg-mk1"))&&IsElementPresent(By.id("reg-mk2"))&&IsElementPresent(By.id("reg-mk3")))==false)
				              flag=1;
				              else if((IsElementPresent(By.id("reg-mk1"))&&IsElementPresent(By.id("reg-mk2"))&&IsElementPresent(By.id("reg-mk3")))==true)
				                 if((driver.findElement(By.id("reg-mk1")).isDisplayed())&&(driver.findElement(By.id("reg-mk2")).isDisplayed())&&(driver.findElement(By.id("reg-mk3")).isDisplayed()))  
				                  {
				                	 driver.findElement(By.id("reg-mk1")).clear();
				                   driver.findElement(By.id("reg-mk1")).sendKeys(data);
				                   driver.findElement(By.id("reg-mk2")).clear();
				                   driver.findElement(By.id("reg-mk2")).sendKeys(Constants.AccountNoReferrer);
				                   driver.findElement(By.id("reg-mk3")).clear();
				                   //if(driver.findElement(By.id("reg-mk3")).getAttribute("placeholder").contains("Date"))
				                  if(MatchKeyName_3.contains("Date"))
				                    {
				                     driver.findElement(By.id("reg-mk3")).sendKeys(Constants.date);
				                     Constants.MatchKey=strDate;
				                    }
				                   else if(MatchKeyName_3.contains("ZipCode"))
				                     {
				                      driver.findElement(By.id("reg-mk3")).sendKeys(Constants.Pin);
				                      Constants.MatchKey=strPIN;
				                     }
				                   Constants.AccountNumFilled="Y";
				                  }
				                 else APP_LOGS.debug("Account Number field (matchkey) are not displayed");
				                else APP_LOGS.debug("Account Number field (matchkey) are not present");
				             
				            else if(flag==0)
				                if(driver.findElement(By.name(OR.getProperty(object))).isDisplayed())
				                {
				                	driver.findElement(By.name(OR.getProperty(object))).clear();
				                 driver.findElement(By.name(OR.getProperty(object))).sendKeys(strPIN);
				                 Constants.MatchKey=strPIN;
				                 Constants.AccountNumFilled="Y";
				                }
				                else APP_LOGS.debug("Account Number field (non-matchkey) is not displayed");
				           if(flag==1)
				           {
				            String PhoneNumResult=writePhoneNum();
				           APP_LOGS.debug(PhoneNumResult);
				           }
				          
				           else
				           {
				            APP_LOGS.debug("Account Number field (non-matchkey) is not displayed");           
				            return Constants.KEYWORD_PASS;
				           }
				         
				        }catch(Exception e){
				        
				         return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
				        } 
				       return Constants.KEYWORD_PASS;
				     }
			      
			      public String switch_window(String object, String data)
					{
						String last_opened;
						APP_LOGS.debug("Switching the window handle");
						Set<String> windows=driver.getWindowHandles();
						Iterator<String> iter=windows.iterator();
						while(iter.hasNext())
						{
							 last_opened = iter.next();
						
						driver.switchTo().window(last_opened);
						}
						return Constants.KEYWORD_PASS+"   Successfully switched the window";
						
					}

			 //**************************************mailtrap**************************     
			      public  org.jsoup.nodes.Document GetMailTrapMsgAPI(String URI, String SearchParamtr){
				  APP_LOGS.debug("Retrieving message");
		    int flag=0;
		    long initialTime= System.currentTimeMillis();
		    
				  org.jsoup.nodes.Document doc=null;
				  try{
					  	String SearchParameter=SearchParamtr;
					  	String MailTrapURI=URI;
					  
					  	do{
					  		URL url = new URL(MailTrapURI);
					
					  		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					  		conn.setRequestMethod("GET");
					  		conn.setRequestProperty("Accept", "application/xml");
					  		if (conn.getResponseCode() != 200) 
					  			{
					  				throw new RuntimeException("Failed : HTTP error code : "
									+ conn.getResponseCode());
					  			}
				 
					  		BufferedReader br = new BufferedReader(new InputStreamReader(
							(conn.getInputStream())));
					  		String output="";
					  		String outputshow;				
					  		while (( outputshow=br.readLine()) != null) 
					  			{
					  				output=output.concat(outputshow);							
					  			}
					  		conn.disconnect();
					  		String path = System.getProperty("user.dir")+"\\src\\XMLfile.txt";
					  		File file = new File(path);
					  		if(file.exists())
					  			file.delete();
						
					  		RandomAccessFile raf = new RandomAccessFile(file, "rw");
			        		raf.seek(0);
			        		raf.write(output.getBytes());
			        		raf.close();
					
			        		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			        		DocumentBuilder db = dbf.newDocumentBuilder();
			        		Document dom = db.parse(file);
			        		file.delete();
			        		Element docEle = dom.getDocumentElement();
			        		NodeList nl = docEle.getElementsByTagName("message");
			        		if(nl.getLength()==0)
								{
			        			flag=1;
			        			Thread.sleep(5000L);
							
								}
					
			        		if(nl.getLength()==0&&initialTime+120000L<=System.currentTimeMillis())
			        		{         
				  
		//	        			System.out.println("URI does not returns any results");
			        			return doc;
			        		}
						
			        		else if(nl.getLength()!=0)
			        		{  
			        			flag=0;
			        			String[] HTMLpath=new String[nl.getLength()];
			        			File[] HTMLfile = new File[nl.getLength()];
						
			        			RandomAccessFile[] rafHTML=new RandomAccessFile[nl.getLength()];
			        			for(int EmailIndex=0;EmailIndex<nl.getLength();EmailIndex++)
			        			{ 
			        				int EmailFlag=0;
			        				Element el = (Element)nl.item(EmailIndex);
			        				HTMLpath[EmailIndex] = System.getProperty("user.dir")+"\\src\\HTMLfile"+EmailIndex+".txt";
			        				HTMLfile[EmailIndex] = new File(HTMLpath[EmailIndex]);
			        				rafHTML[EmailIndex] = new RandomAccessFile(HTMLfile[EmailIndex], "rw");
			        				rafHTML[EmailIndex].seek(0);
			        				rafHTML[EmailIndex].writeBytes(el.getTextContent());
			        				rafHTML[EmailIndex].close();
			        				Scanner scanner = new Scanner(HTMLfile[EmailIndex]);

							    //now read the file line by line...
			        				int lineNum = 0;
			        				while (scanner.hasNextLine()) 
			        				{
			        					String line = scanner.nextLine();
			        					lineNum++;
							        if(line.contains(SearchParameter))
							        	{ 
							            	EmailFlag=1;
							            	ReqHTMLIndex=EmailIndex;
							        	}

			        				}
			        				scanner.close();
			        				if(EmailFlag==1)
			        				{
			        					doc = Jsoup.parse(HTMLfile[ReqHTMLIndex], "UTF-8"); 

			        					HTMLfile[ReqHTMLIndex].delete();
			        					flag=2;
									
			        				}
			        				else if(EmailFlag!=1)
			        					HTMLfile[EmailIndex].delete();
			        			}
							
			        			if(flag!=2)
			        				Thread.sleep(5000L);
							
			        		}
					  	}while(flag!=2&&initialTime+120000L>System.currentTimeMillis());
					  	if(flag!=2&&initialTime+120000L<System.currentTimeMillis())
					  		APP_LOGS.debug("Required email is not found in the MailTrap within 2 minutes...");
						
				  }
				  catch(Exception e){
					  e.printStackTrace();
				   
				   }
				  return doc;
				}
		   
			      public  String[] SearchOccurenceOfWordInExtireString(String string, String[] WordArray){
					  APP_LOGS.debug("Searching occurence of word in the entire string:");
					  
						  String[] MatchedString = new String[WordArray.length];
						  int MatchCounter=0;
						  String[] TitleToBeSearchedArray=WordArray;
						  String GivenString=string;
						  int[] NoOfWords=new int[WordArray.length];
						  for(int i=0; i<TitleToBeSearchedArray.length;i++)
						  {
							  String TitleToBeSearched=TitleToBeSearchedArray[i];
							  if(TitleToBeSearched.isEmpty())
						    	  {

								  MatchedString[MatchCounter]="The text: "+TitleToBeSearchedArray[i]+" is empty, hence cannot be searched"; 
								  MatchCounter++;
						    	  }
						      else
						      {

						    	  int WordSize= TitleToBeSearched.length();
							  		for(int LetterCount=0;LetterCount<=GivenString.length();)
							  			{
							  				if(GivenString.indexOf(TitleToBeSearched,LetterCount)!=-1)
							  					{
							  			  			LetterCount=GivenString.indexOf(TitleToBeSearched,LetterCount)+WordSize;
							  			  			NoOfWords[i]++;
							  		  			}
							  				else LetterCount++;
							  			}
							  		if(NoOfWords[i]!=0)
							  		{
							  			MatchedString[MatchCounter]= "The text: "+TitleToBeSearchedArray[i]+" matches with expected text";
							  			MatchCounter++;

							  		}
							  			
							  		else
							  		{

							  			MatchedString[MatchCounter]= "The text: "+TitleToBeSearchedArray[i]+" does not found in the given string";
							  			MatchCounter++;
							  		}
							  }
							  		
							 
						  }
						  //System.out.println("Matched string is: "+MatchedString.toString());
							 return MatchedString;
					}
		    public String VerifyRewardQuantityRefereeAPI(String object,String data){  
				  APP_LOGS.debug("Verifying the quantity");
				  try{
					  String SearchResultToReturn="";
					  Properties properties =LoadProperties();
					  String RefereeBaseRewardType=properties.getProperty("RefereeBaseRewardType");
		              String RefereeRewardAmount=properties.getProperty("RefereeRewardAmount");
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String MailTrapEmailSubjectCongratulations=properties.getProperty("MailTrapEmailSubjectCongratulations");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+Constants.EmailSignup+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI, MailTrapEmailSubjectCongratulations); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes... ";
		              else
		            	  {
		            	  	String text = docHTML.body().text(); 
		            	  	String[] WordsToBeSearched={RefereeRewardAmount,RefereeBaseRewardType};
		            	  	String[] SearchResult=SearchOccurenceOfWordInExtireString(text,WordsToBeSearched);
		            	  	for(int SearchResultCounter=0;SearchResultCounter<SearchResult.length;SearchResultCounter++)
		            	  	{
		            	  		SearchResultToReturn+="Parameter: "+WordsToBeSearched[SearchResultCounter]+" Search Result: "+SearchResult[SearchResultCounter]+"\n";
		            	  	}
		            		if(SearchResultToReturn.contains("does not"))
			            	  	return Constants.KEYWORD_FAIL+" -- " + SearchResultToReturn;
			            	  	else
			            	  		return Constants.KEYWORD_PASS+" -- " + SearchResultToReturn;
		            	  }
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				 }
		    public String VerifyRewardQuantityReferrerAPI(String object,String data){  
				  APP_LOGS.debug("Verifying the quantity");
				  try{
					  String SearchResultToReturn="";
					  Properties properties =LoadProperties();
					  String ReferrerBaseRewardType=properties.getProperty("ReferrerBaseRewardType");
		              String ReferrerRewardAmount=properties.getProperty("ReferrerRewardAmount");
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String MailTrapEmailSubjectCongratulations=properties.getProperty("MailTrapEmailSubjectCongratulations");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+data+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI, MailTrapEmailSubjectCongratulations); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes... ";
		              else
		            	  {
		            	  	String text = docHTML.body().text(); 
		            	  	String[] WordsToBeSearched={ReferrerRewardAmount,ReferrerBaseRewardType};
		            	  	String[] SearchResult=SearchOccurenceOfWordInExtireString(text,WordsToBeSearched);
		            	  	for(int SearchResultCounter=0;SearchResultCounter<SearchResult.length;SearchResultCounter++)
		            	  	{
		            	  		SearchResultToReturn+="Parameter: "+WordsToBeSearched[SearchResultCounter]+" Search Result: "+SearchResult[SearchResultCounter]+"\n";
		            	  	}
		            		if(SearchResultToReturn.contains("does not"))
			            	  	return Constants.KEYWORD_FAIL+" -- " + SearchResultToReturn;
			            	  	else
			            	  		return Constants.KEYWORD_PASS+" -- " + SearchResultToReturn;
		            	  }
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				 }
		    
		    public String NavigateUnlockOfferPageAPI(String object,String data){  
				  APP_LOGS.debug("Verifying the quantity");
				  try{
					  Properties properties =LoadProperties();
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String MailTrapEmailSubjectJoinReferral=properties.getProperty("MailTrapEmailSubjectJoinReferral");
		              String UnlockOfferText=properties.getProperty("UnlockOfferText");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+Constants.EmailSignup+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI,MailTrapEmailSubjectJoinReferral); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes...";
		              else
		            	  {
		            	  	List<org.jsoup.nodes.Element> UnlockOfferHref = docHTML.select("a");
		            	  	for(int i=0; i<UnlockOfferHref.size(); i++)
		            	  	{
		            		  	if(UnlockOfferHref.get(i).text().trim().equalsIgnoreCase(UnlockOfferText))
		            		  	{
		            		  		driver.get(UnlockOfferHref.get(i).attr("href"));
		            		  		break;
		            		  	}
		            	  	} 
		            	  }
		            	  
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				  return Constants.KEYWORD_PASS;
				 }
		    
		    public String VerbalReferralConfirmation_Confirm(String object,String data){  
				  APP_LOGS.debug("Verifying the quantity");
				  try{
					  Properties properties =LoadProperties();
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String MailTrapEmailSubjectConfirmation=properties.getProperty("MailTrapEmailSubjectConfirmation");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+data+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI,MailTrapEmailSubjectConfirmation); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes...";
		              else
		            	  {
		            	  	List<org.jsoup.nodes.Element> ConfirmationHref = docHTML.select("a");
		            	  	for(int i=0; i<ConfirmationHref.size(); i++)
		            	  	{
		            		  	if(ConfirmationHref.get(i).text().trim().equalsIgnoreCase("Confirm"))
		            		  	{
		            		  		driver.get(ConfirmationHref.get(i).attr("href"));
		            		  		break;
		            		  	}
		            	  	} 
		            	  }
		            	  
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				  return Constants.KEYWORD_PASS;
				 }
		    public String VerbalReferralConfirmation_Deny(String object,String data){  
				  APP_LOGS.debug("Verifying the quantity");
				  try{
					  Properties properties =LoadProperties();
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String MailTrapEmailSubjectConfirmation=properties.getProperty("MailTrapEmailSubjectConfirmation");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+data+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI,MailTrapEmailSubjectConfirmation); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes...";
		              else
		            	  {
		            	  	List<org.jsoup.nodes.Element> ConfirmationHref = docHTML.select("a");
		            	  	for(int i=0; i<ConfirmationHref.size(); i++)
		            	  	{
		            		  	if(ConfirmationHref.get(i).text().trim().equalsIgnoreCase("Deny"))
		            		  	{
		            		  		driver.get(ConfirmationHref.get(i).attr("href"));
		            		  		break;
		            		  	}
		            	  	} 
		            	  }
		            	  
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				  return Constants.KEYWORD_PASS;
				 }
		    
		    public String ReferralDenialReferrerAPI(String object,String data){  
				  APP_LOGS.debug("Verifying the quantity");
				  try{
					  String SearchResultToReturn="";
					  Properties properties =LoadProperties();
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String MailTrapEmailSubjectNotification=properties.getProperty("MailTrapEmailSubjectNotification");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+data+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI, MailTrapEmailSubjectNotification); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes...";
		              else
		            	  {
		            	  	String text = docHTML.body().text(); 
		            	  	String[] WordsToBeSearched={MailTrapEmailSubjectNotification};
		            	  	String[] SearchResult=SearchOccurenceOfWordInExtireString(text,WordsToBeSearched);
		            		for(int SearchResultCounter=0;SearchResultCounter<SearchResult.length;SearchResultCounter++)
		            	  	{
		            	  		SearchResultToReturn+=SearchResult[SearchResultCounter]+"\n";
		            	  	}
		            	  	return Constants.KEYWORD_PASS+" -- "+SearchResultToReturn;
		            	  	//return SearchResult;
		            	  }
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				 }
		    public String ReferralDenialRefereeAPI(String object,String data){  
				  APP_LOGS.debug("Verifying the quantity");
				  try{
					  String SearchResultToReturn="";
					  Properties properties =LoadProperties();
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String MailTrapEmailSubjectNotification=properties.getProperty("MailTrapEmailSubjectNotification");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+Constants.EmailSignup+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI, MailTrapEmailSubjectNotification); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes...";
		              else
		            	  {
		            	  	String text = docHTML.body().text(); 
		            	  	String[] WordsToBeSearched={MailTrapEmailSubjectNotification};
		            	  	String[] SearchResult=SearchOccurenceOfWordInExtireString(text,WordsToBeSearched);
		            	  	for(int SearchResultCounter=0;SearchResultCounter<SearchResult.length;SearchResultCounter++)
		            	  	{
		            	  		SearchResultToReturn+=SearchResult[SearchResultCounter]+"\n";
		            	  	}
		            	  	return Constants.KEYWORD_PASS+" -- "+SearchResultToReturn;
		            	  	//return SearchResult;
		            	  }
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				 }
		    
		    
		    public String randomDataOfBirth(int yearStart, int yearEnd){
		        GregorianCalendar gc = new GregorianCalendar();
		        int year = randBetween(yearStart, yearEnd);
		        gc.set(Calendar.YEAR, year);
		         int dayOfYear = randBetween(1, gc.getActualMaximum(Calendar.DAY_OF_YEAR));

		            gc.set(Calendar.DAY_OF_YEAR, dayOfYear);
		            String date = null;
		            if(gc.get(Calendar.MONTH) == 0)
		            { 
		                 date =  "01" +gc.get(Calendar.DAY_OF_MONTH)+ "" + gc.get(Calendar.YEAR);
		                 if(gc.get(Calendar.DAY_OF_MONTH)<10)
		                	 date =  "01" +"0"+gc.get(Calendar.DAY_OF_MONTH)+ "" + gc.get(Calendar.YEAR);
		                	    	 
		            }else if(gc.get(Calendar.MONTH)> 0)
		            {     
		                 date = gc.get(Calendar.MONTH)+"" +gc.get(Calendar.DAY_OF_MONTH) + "" +  gc.get(Calendar.YEAR);
		                 if((gc.get(Calendar.MONTH)<10) &&(gc.get(Calendar.DAY_OF_MONTH)<10))
		                	 
		                	 date = "0"+gc.get(Calendar.MONTH) +"0"+gc.get(Calendar.DAY_OF_MONTH) + "" +  gc.get(Calendar.YEAR);
		                	
		                 else if(gc.get(Calendar.MONTH)<10)
		                	 
		                	 date = "0"+gc.get(Calendar.MONTH)+"" +gc.get(Calendar.DAY_OF_MONTH) + "" +  gc.get(Calendar.YEAR);
		                	 
		                 else if(gc.get(Calendar.DAY_OF_MONTH)<10)
		                	 
		                	 date = gc.get(Calendar.MONTH) +"0"+gc.get(Calendar.DAY_OF_MONTH) + "" +  gc.get(Calendar.YEAR);
		                	 
		                	 
		                 
		                	 
		            }
		            return date;    
		    }

		    private int randBetween(int start, int end) {
		        return start + (int)Math.round(Math.random() * (end - start));
		    }
		    
	
		    
		    public  String writeAccountNum(String object,String data){
				  APP_LOGS.debug("Writing last name random account number(last 4 digits) and random pin");
				  
				  try{
					  
					  Thread.sleep(2000L);
					 Properties properties=LoadProperties();
					 String EmailUsedAsAccountNumber =properties.getProperty("EmailUsedAsAccountNumber");
			      String AccountNumberType =properties.getProperty("AccountNumberType(MatchKeyOrPhoneNumber)");
			      if (AccountNumberType.equalsIgnoreCase("Matchkey"))
			           
			      {
			    	  
				  if(Constants.AccountNumFilled=="Y")
					  return Constants.KEYWORD_PASS;
				  else
				     { 
			    	  String MatchKeyStr=WriteMatchkey(data);
			    	        Constants.MatchKey=MatchKeyStr;
			           }
			      }
			    	  else if(AccountNumberType.equalsIgnoreCase("PhoneNumber"))
				      {  
			    		  if(Constants.PhoneNumFilled=="Y")
			    			  return Constants.KEYWORD_PASS;
			    		  else
			    		  {
				             Constants.RefereePhoneNumber=String.valueOf((long)(Math.random()*100000 + 3333300000L) );
				             driver.findElement(By.id(OR.getProperty(object))).clear();
				             driver.findElement(By.id(OR.getProperty(object))).sendKeys(Constants.RefereePhoneNumber);
			    		  }
				      }
			    	  else if (EmailUsedAsAccountNumber.equalsIgnoreCase("yes"))
			    	  {
			    		  Constants.Referrer="test"+Integer.toString((int)(Math.random()*10000))+"@test"+Integer.toString((int)(Math.random()*10000))+".com";
			    		  driver.findElement(By.id(OR.getProperty(object))).clear();
			    		  driver.findElement(By.id(OR.getProperty(object))).sendKeys(Constants.Referrer);
			    	  }
				  }	
			       		catch(Exception E){
			       			
					return Constants.KEYWORD_FAIL + " Unable to write " + E.getMessage();
				
			            }
			       	return Constants.KEYWORD_PASS;		
			}
		 
		 public String WriteMatchkey(String data){
		       
		       String strPIN;
		      String strDate;
		      Properties properties =LoadProperties();
		              String MatchKeyName_3=properties.getProperty("MatchKeyName_3");
		      Constants.AccountNoReferee=String.valueOf((int)(1000+ Math.random()*8999));
		          Constants.Pin=String.valueOf((int)(10000+ Math.random()*89999));
		          Constants.date=randomDataOfBirth(1914, 2010);
		          
		          strPIN = data+"|"+Constants.AccountNoReferee+"|"+Constants.Pin;
		         strDate = data+"|"+Constants.AccountNoReferee+"|"+Constants.date;
		        String AccountNumField="9000000663#client.accnt.number";
		         if(IsElementPresent(By.name(AccountNumField))==true)
		          if(!driver.findElement(By.name(AccountNumField)).isDisplayed())
		             
		               {  driver.findElement(By.id("profile-mk1")).clear();
		                  driver.findElement(By.id("profile-mk1")).sendKeys(data);
		                  driver.findElement(By.id("profile-mk2")).clear();
		                     driver.findElement(By.id("profile-mk2")).sendKeys( Constants.AccountNoReferee);
		                     driver.findElement(By.id("profile-mk3")).clear();
		                     if(MatchKeyName_3.contains("Date"))

		           {
		            driver.findElement(By.id("profile-mk3")).sendKeys(Constants.date);
		            return strDate;
		           }
		          else if(MatchKeyName_3.contains("ZipCode"))
		            {
		             driver.findElement(By.id("profile-mk3")).sendKeys(Constants.Pin);
		             return strPIN;
		            }

		               }
		          else if(IsElementPresent(By.name(AccountNumField))==false)
		          {         driver.findElement(By.id("profile-mk1")).clear();
		                   driver.findElement(By.id("profile-mk1")).sendKeys(data);
		                   driver.findElement(By.id("profile-mk2")).clear();
		                      driver.findElement(By.id("profile-mk2")).sendKeys( Constants.AccountNoReferee);
		                      driver.findElement(By.id("profile-mk3")).clear();
		                      if(MatchKeyName_3.contains("Date"))
		            {
		             driver.findElement(By.id("profile-mk3")).sendKeys(Constants.date);
		             return strDate;
		            }
		           else if(MatchKeyName_3.contains("ZipCode"))
		             {
		              driver.findElement(By.id("profile-mk3")).sendKeys(Constants.Pin);
		              return strPIN;
		             }
		                }
		              else {
		            	  driver.findElement(By.name(OR.getProperty(AccountNumField))).clear();
		            	  driver.findElement(By.name(OR.getProperty(AccountNumField))).sendKeys(strPIN);
		              }
		               
		       
		      return strPIN;
		      }
		public boolean AreElementsPresent(By by) {
		         try {
		           driver.findElements(by);
		           return true;
		         } catch (org.openqa.selenium.NoSuchElementException e) {
		           return false;
		         }
		     }
		
		
		
		 public String writeRefereePurchaseImportCSV(String object,String data) {
	          try { 
	        	  int flag=0;
	        	  APP_LOGS.debug("Writing referee import csv");
	           String[] p=data.split(",");
	           String path = System.getProperty("user.dir")+"\\src\\RefereePurchaseImport.csv";
	           Constants.RefereePurchaseImportPath=path;
	           Properties properties=LoadProperties();
	        String Answer=properties.getProperty("EmailUsedAsAccountNumber");
	        String OfferExpiryPeriod=properties.getProperty("OfferExpiryPeriod");
	        String MaximumDaysBetweenOpeningAccountAndRegistration=properties.getProperty("MaximumDaysBetweenOpeningAccountAndRegistration");
	        String MaxDaysFromReferralToFirstPurchase=properties.getProperty("MaxDaysFromReferralToFirstPurchase");
	        String ProductCode=properties.getProperty("ProductCode");
	        String ProductCodenoteligible=properties.getProperty("ProductCodenoteligible");
	        String PurchaseAmountMinimumForReward=properties.getProperty("PurchaseAmountMinimumForReward");
	           FileWriter writer = new FileWriter(path);
	           PrintWriter pw = new PrintWriter(writer);
	          
	           pw.print("Purchase Number");
	           writer.write(',');
	           pw.print("Account Number");
	           writer.write(',');
	           pw.print("Purchase Date");
	           writer.write(',');
	           pw.print("Product Code");
	           writer.write(',');
	           pw.println("Total Dollars Spent");
	           //writer.write('\n');
	           String randPurchaseNumber=Long.toString((long)(100000000000L+(Math.random()*899999999999L)));
	           writer.write(randPurchaseNumber);
	           writer.write(',');
	           
	           
	             
	             String AccountNumberType =properties.getProperty("AccountNumberType(MatchKeyOrPhoneNumber)");
	             if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("Matchkey"))
	               {
	            	 	writer.write(Constants.MatchKey);
	          
	               }
	             	else if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("PhoneNumber"))
	             
	             		{ 
	            	 		writer.write(Constants.RefereePhoneNumber);
	                
	             		}

	             		else if(Answer.equalsIgnoreCase("Yes"))
	             			{
	             				writer.write(Constants.Referee);
	              
	              
	             			}
	             
	             
	             writer.write(',');
	             String pattern = "MM/dd/yyyy";
	             SimpleDateFormat format = new SimpleDateFormat(pattern);
	             // formatting
	             String date=format.format(new Date());
	             
	            
	             Calendar cal = Calendar.getInstance();
	             
	             if(p[0].equalsIgnoreCase("OfferExpiryPeriod"))
	             {
	            	 int DaysToBeAdded = Integer.valueOf(OR.getProperty(object))+Integer.valueOf(OfferExpiryPeriod);
	            	 
	            	 cal.add(Calendar.DATE, DaysToBeAdded);
	             }
	             	else if(p[0].equalsIgnoreCase("MaxDaysFromReferralToFirstPurchase"))
	             	{
	             		int DaysToBeAdded = Integer.valueOf(OR.getProperty(object))+Integer.valueOf(MaxDaysFromReferralToFirstPurchase);
	             
	             		cal.add(Calendar.DATE, DaysToBeAdded);
	             	}
	             
	             		else if(p[0].equalsIgnoreCase("MaximumDaysBetweenOpeningAccountAndRegistration"))
	             			{
	             				int DaysToBeAdded = Integer.valueOf(OR.getProperty(object))+Integer.valueOf(MaximumDaysBetweenOpeningAccountAndRegistration);
	              
	             					cal.add(Calendar.DATE, -DaysToBeAdded);
	             			}
	             		else if(p[0].isEmpty())
			              {
	             			flag=1;
			            	  writer.write(',');
			            	  
				              
			              }
	             			else
	             			
	             				cal.add(Calendar.DATE, Integer.parseInt(p[0]));
	             if(flag==0)
	             {			
	            	 Date NewDate = cal.getTime();    
	              				String ModifiedDate = format.format(NewDate);
	              
	              				writer.write(ModifiedDate);
	              				writer.write(',');
	             			}
	              				
	              				if(p[1].equalsIgnoreCase("ProductCode"))
	              				{
	             
	            
	              					if(ProductCode.isEmpty())
	              					{ 
	              						writer.flush();
	         
	              						writer.close();
	              
	           
	              					}
	              					else
	              						{
	              						writer.write(ProductCode);
	              						writer.write(',');
	              						if(p[2].equalsIgnoreCase("PurchaseAmountMinimumForReward"))
	              						{
	        
	              							if (PurchaseAmountMinimumForReward.isEmpty())
	              							{
	              								writer.flush();
	         
	              								writer.close();
	              
	             
	              							}
	              							else
	              							{
	              								int amount = Integer.valueOf(PurchaseAmountMinimumForReward)+Integer.valueOf(p[3]);
	         
	              								writer.write(String.valueOf(amount));
	   
	              								writer.flush();
	              								writer.close();
	       
	              							}
	              						}
	              						else
	              							{
	              								writer.flush();
	         
	              								writer.close();
	              
	              							}
	              						}
	              				}
	              				
	              				else if(p[1].equalsIgnoreCase("ProductCodenoteligible"))
	              				{
	              		             
	              		            
	              					if(ProductCodenoteligible.isEmpty())
	              					{ 
	              						writer.flush();
	         
	              						writer.close();
	              
	           
	              					}
	              					else
	              						{
	              						writer.write(ProductCodenoteligible);
	              						writer.write(',');
	              						if(p[2].equalsIgnoreCase("PurchaseAmountMinimumForReward"))
	              						{
	        
	              							if (PurchaseAmountMinimumForReward.isEmpty())
	              							{
	              								writer.flush();
	         
	              								writer.close();
	              
	             
	              							}
	              							else
	              							{
	              								int amount = Integer.valueOf(PurchaseAmountMinimumForReward)+Integer.valueOf(p[3]);
	         
	              								writer.write(String.valueOf(amount));
	   
	              								writer.flush();
	              								writer.close();
	       
	              							}
	              						}
	              						else
	              							{
	              								writer.flush();
	         
	              								writer.close();
	              
	              							}
	              						}
	              				}
	              					
	            return Constants.KEYWORD_PASS;
	             } catch(Exception e){
	         return Constants.KEYWORD_FAIL+" -- Unable to write file "+e.getMessage();
	              }
	      }
	      public String writeReferrerPurchaseImportCSV(String object,String data) {
	          try { APP_LOGS.debug("Writing referee import csv");
	           String[] p=data.split(",");
	           String path = System.getProperty("user.dir")+"\\src\\ReferrerPurchaseImport.csv";
	           Constants.ReferrerPurchaseImportPath=path;
	           Properties properties=LoadProperties();
	        String Answer=properties.getProperty("EmailUsedAsAccountNumber");
	        String OfferExpiryPeriod=properties.getProperty("OfferExpiryPeriod");
	        String MaximumDaysBetweenOpeningAccountAndRegistration=properties.getProperty("MaximumDaysBetweenOpeningAccountAndRegistration");
	        String MaxDaysFromReferralToFirstPurchase=properties.getProperty("MaxDaysFromReferralToFirstPurchase");
	        String ProductCode=properties.getProperty("ProductCode");
	        String PurchaseAmountMinimumForReward=properties.getProperty("PurchaseAmountMinimumForReward");
	           FileWriter writer = new FileWriter(path);
	           PrintWriter pw = new PrintWriter(writer);
	          
	           pw.print("Purchase Number");
	           writer.write(',');
	           pw.print("Account Number");
	           writer.write(',');
	           pw.print("Purchase Date");
	           writer.write(',');
	           pw.print("Product Code");
	           writer.write(',');
	           pw.println("Total Dollars Spent");	          
	           String randPurchaseNumber=Long.toString((long)(100000000000L+(Math.random()*899999999999L)));	        
	           writer.write(randPurchaseNumber);
	           writer.write(',');	             
	             String AccountNumberType =properties.getProperty("AccountNumberType(MatchKeyOrPhoneNumber)");
	             if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("Matchkey"))
	               {
	               writer.write(Constants.MatchKey);
	          
	               }
	             else if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("PhoneNumber"))
	             
	             { 
	              writer.write(Constants.RefereePhoneNumber);
	                
	             }
	          	             else if(Answer.equalsIgnoreCase("Yes"))
	             {
	              writer.write(Constants.Referrer);              
	             }             
	             writer.write(',');
	             String pattern = "MM/dd/yyyy";
	             SimpleDateFormat format = new SimpleDateFormat(pattern);
	             // formatting
	             String date=format.format(new Date());
	             
	            
	             Calendar cal = Calendar.getInstance();
	             
	             if(p[0].equalsIgnoreCase("OfferExpiryPeriod"))
	             {
	             int DaysToBeAdded = Integer.valueOf(OR.getProperty(object))+Integer.valueOf(OfferExpiryPeriod);
	             
	              cal.add(Calendar.DATE, DaysToBeAdded);
	             }
	             else if(p[0].equalsIgnoreCase("MaxDaysFromReferralToFirstPurchase"))
	             {
	              int DaysToBeAdded = Integer.valueOf(OR.getProperty(object))+Integer.valueOf(MaxDaysFromReferralToFirstPurchase);
	             
	               cal.add(Calendar.DATE, DaysToBeAdded);
	             }
	             
	             else if(p[0].equalsIgnoreCase("MaximumDaysBetweenOpeningAccountAndRegistration"))
	             {
	              int DaysToBeAdded = Integer.valueOf(OR.getProperty(object))+Integer.valueOf(MaximumDaysBetweenOpeningAccountAndRegistration);
	              
	               cal.add(Calendar.DATE, -DaysToBeAdded);
	             }
	             else
	              
	             cal.add(Calendar.DATE, Integer.parseInt(p[0]));
	              Date NewDate = cal.getTime();    
	              String ModifiedDate = format.format(NewDate);
	              
	             writer.write(ModifiedDate);
	             writer.write(',');
	              
	             if(p[1].equalsIgnoreCase("ProductCode"))
	             {	            
	              if(ProductCode.isEmpty())
	              { 
	               writer.flush();
	         
	              writer.close();
	              
	           
	              }
	        else
	        {
	             writer.write(ProductCode);
	             writer.write(',');
	             if(p[2].equalsIgnoreCase("PurchaseAmountMinimumForReward"))
	             {
	        
	        if (PurchaseAmountMinimumForReward.isEmpty())
	         {
	         writer.flush();
	         
	              writer.close();
	              
	             
	         }
	        else
	        {
	         int amount = Integer.valueOf(PurchaseAmountMinimumForReward)+Integer.valueOf(p[3]);
	         
	             writer.write(String.valueOf(amount));
	   
	             writer.flush();
	             writer.close();
	       
	        }
	             }else
	             {
	              writer.flush();
	         
	              writer.close();
	              
	           
	             }
	        }
	             }
	            return Constants.KEYWORD_PASS;
	             } catch(Exception e){
	         return Constants.KEYWORD_FAIL+" -- Unable to write file "+e.getMessage();
	              }
	      }
		    
			 public  String writeIntUsingJSByID(String object,String data){
	     APP_LOGS.debug("Writing in text box");
	     
	     try{
	      driver.findElement(By.id(OR.getProperty(object))).click();
	     // driver.findElement(By.id(object)).clear();
	      JavascriptExecutor jse = (JavascriptExecutor) driver;
	      jse.executeScript("document.getElementById('"+OR.getProperty(object)+"').value = '"+data+"';");
	      
	     }catch(Exception e){
	      return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

	     }
	     return Constants.KEYWORD_PASS;
	     
	    }
	       
	       public  String SelectItemFromDropdown(String object,String data){
	       APP_LOGS.debug("Writing random integer in text box for  the pincode of the referrer/referee");
	       try{
	    	   Properties properties=LoadProperties();
          	 String ClientRequiresAddress=properties.getProperty("ClientRequiresAddress"); 
	       
	        List<WebElement> ListItems=driver.findElement(By.id(OR.getProperty(object))).findElements(By.tagName("option"));
	        for(int i=0; i<ListItems.size();i++)
	        {
	         if(ListItems.get(i).getText().equalsIgnoreCase(data))
	          {
	           driver.findElement(By.id(OR.getProperty(object))).sendKeys((ListItems.get(i).getText()));
	           break;
	          }
	        }
	        
	        
	        return Constants.KEYWORD_PASS;
	       }catch(Exception e){
	        return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
	       }    
	    }
	   
	     public  String writePurchaseFileNameReferrer(String object,String data){
			    APP_LOGS.debug("Writing in text box");
			    
			    try{
			     driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.ReferrerPurchaseImportPath);
			    }catch(Exception e){
			     return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

			    }
			    return Constants.KEYWORD_PASS;
			    
			   }
	
	     
	     public  String writeMemberImportFileNameReferee(String object,String data){
	         APP_LOGS.debug("Writing in text box");
	         
	         try{
	          driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.RefereeImportCSV);
	         }catch(Exception e){
	          return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

	         }
	         return Constants.KEYWORD_PASS;
	         
	        }
	     
	     
	     public  String writePurchaseFileNameReferee(String object,String data){
			    APP_LOGS.debug("Writing in text box");
			    
			    try{
			     driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.RefereePurchaseImportPath);
			    }catch(Exception e){
			     return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();

			    }
			    return Constants.KEYWORD_PASS;
			    
			   }
	     public  String DeleteCookies(String object,String data){
	         APP_LOGS.debug("Dleting all cookies");
	         
	         try{
	          driver.manage().deleteAllCookies();
	         }catch(Exception e){
	          return Constants.KEYWORD_FAIL+" Unable to delete cookies "+e.getMessage();

	         }
	         return Constants.KEYWORD_PASS;
	         
	        }
//********************************************Pending Denial Keywords****************************************************
	     
	     
	     public String writeRefereeImportCSV(String object,String data) {
             try { 
            	 APP_LOGS.debug("Writing referee import csv");
            	 String[] p=data.split(",");
            	 String path = System.getProperty("user.dir")+"\\src\\RefereeImportCSV.csv";
            	 FileWriter writer = new FileWriter(path);
            	 Constants.RefereeImportCSV=path;
            	 Properties properties=LoadProperties();
            	 String Answer=properties.getProperty("EmailUsedAsAccountNumber");
            	 String ProductCode=properties.getProperty("ProductCode");
            	 String ProductCodenoteligible=properties.getProperty("ProductCodenoteligible");
            	 String PurchaseAmountMinimumForReward=properties.getProperty("PurchaseAmountMinimumForReward");
            	 String AccountNumberType =properties.getProperty("AccountNumberType(MatchKeyOrPhoneNumber)");
            	 writer.write("Account Number");
            	 writer.write('\n');
            	 if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("Matchkey"))
            		 writer.write(Constants.MatchKey);
            	 else if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("PhoneNumber"))
            		 writer.write(Constants.RefereePhoneNumber);
            	 else
            		 writer.write(Constants.Referee);
            	 writer.write(',');
            	 String pattern = "MM/dd/yyyy";
            	 SimpleDateFormat format = new SimpleDateFormat(pattern);            	            	 
            	 String ModifiedDate = format.format(CCTimeTravelDate);
            	 writer.write(ModifiedDate);      
            	 writer.write(',');
            	 writer.write(p[0]);
            	 for(int i=0; i<12; i++)
            		 writer.write(',');  
            	 if(p[1].equalsIgnoreCase("ProductCode"))
            		 if(!ProductCode.isEmpty())
            			 writer.write(ProductCode);
            	 if(p[1].equalsIgnoreCase("ProductCodenoteligible"))
            		 if(!ProductCodenoteligible.isEmpty())
            			 writer.write(ProductCodenoteligible);
            	 writer.write(',');
            	 if(p[2].equalsIgnoreCase("PurchaseAmountMinimumForReward"))
            		 if (!PurchaseAmountMinimumForReward.isEmpty())
            		 {
            			 int amount = Integer.valueOf(PurchaseAmountMinimumForReward)+Integer.valueOf(p[3]);
            			 writer.write(String.valueOf(amount));
            		 }
            	 writer.flush();
            	 writer.close();          			  
            	 return Constants.KEYWORD_PASS;
             } catch(Exception e){
            	 e.printStackTrace();
            	 return Constants.KEYWORD_FAIL+" -- Unable to write file "+e.getMessage();
             }
	     }

	     
	     @SuppressWarnings("deprecation")
			public String VerifyPendingDenialEmailRefereeAPI(String object,String data){  
				  APP_LOGS.debug("Verifying email content");
				  try{
					  String SearchResultToReturn="";
					  Properties properties =LoadProperties();
					  String Parameter_P=properties.getProperty("Parameter_P");
					  String MailTrapAccountDetailsContent=properties.getProperty("MailTrapAccountDetailsContent");
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+ Constants.EmailSignup+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI, data); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes... ";
		              else
		            	  { 
		                     String Newpattern = "MMMMM dd, yyyy";
		                     String NewPattern = "MMMM d, yyyy";
			                 SimpleDateFormat Newformat = new SimpleDateFormat(Newpattern);
			                 SimpleDateFormat NewFormat = new SimpleDateFormat(NewPattern); 			                
			         //        System.out.println("Date saved in Global Variable: "+CCTimeTravelDate );
			                 Calendar cal = Calendar.getInstance();
			                 cal.setTime(CCTimeTravelDate);		               
			                 cal.add(cal.DATE, Integer.valueOf(Parameter_P));			                
			                 Date NewDate = cal.getTime();
			            //     System.out.println("New Date is: "+NewDate);
			                 String ModifiedDate = "";
			                 
			                 int GetDate = cal.DAY_OF_MONTH;
			                 if(GetDate<=9)
			                 {
			                	 ModifiedDate=NewFormat.format(NewDate);
			                 }
			                 else
			                	 ModifiedDate=Newformat.format(NewDate);
			           //      System.out.println("Modified date is: "+ModifiedDate);		                
		            	  	String text = docHTML.body().text();
		            	  	Elements EditProfile=docHTML.body().getElementsByAttribute("href");
		            	  	for(int i=0;i<EditProfile.size();i++)	
		            	  	{if(EditProfile.get(i).text().contentEquals(MailTrapAccountDetailsContent))
		            	  		{
		            	//  		System.out.println("Value of href is:"+EditProfile.get(i).text());
		            	 // 		System.out.println("URL is: "+EditProfile.get(i).attr("href"));
		            	  		Constants.URL=EditProfile.get(i).attr("href");
		            	  		break;
		            	  		}
		            	  	}
		            	  	String[] WordsToBeSearched={ModifiedDate,MailTrapAccountDetailsContent};
		            	  	String[] SearchResult=SearchOccurenceOfWordInExtireString(text,WordsToBeSearched);
		            	  	for(int SearchResultCounter=0;SearchResultCounter<SearchResult.length;SearchResultCounter++)
		            	  	{
		            	  		SearchResultToReturn+=" Parameter: "+WordsToBeSearched[SearchResultCounter]+" Search Result: "+SearchResult[SearchResultCounter]+"\n";
		            	  	}
		            		if(SearchResultToReturn.contains("does not"))
			            	  	return Constants.KEYWORD_FAIL+ SearchResultToReturn;
			            	  	else
			            	  		return Constants.KEYWORD_PASS+ SearchResultToReturn;
		            	  }
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
				 }
	     
	     @SuppressWarnings("deprecation")
			public String VerifyPendingDenialEmailReferrerAPI(String object,String data){  
				  APP_LOGS.debug("Verifying email content");
				  try{
					  String SearchResultToReturn="";
					  Properties properties =LoadProperties();
					  String Parameter_P=properties.getProperty("Parameter_P");
					  String MailTrapAccountDetailsContent=properties.getProperty("MailTrapAccountDetailsContent");
		              String MailTrapInboxID=properties.getProperty("MailTrapInboxID");
		              String EmailAmendAccountDetails=properties.getProperty("EmailAmendAccountDetails");
		              String URI= "https://mailtrap.io/api/v1/inboxes/"+MailTrapInboxID+"/messages?search="+data+"&page=1&api_token=ab91a2c89fba02f280752a174d38eee2";
		              org.jsoup.nodes.Document docHTML= GetMailTrapMsgAPI(URI, EmailAmendAccountDetails); 
		              if(docHTML==null)
		            	  return Constants.KEYWORD_FAIL+" -- Required email is not found in the MailTrap within 2 minutes... ";
		              else
		              {
		            	  String Newpattern = "MMMMM dd, yyyy";
		                     String NewPattern = "MMMM d, yyyy";

		                 SimpleDateFormat Newformat = new SimpleDateFormat(Newpattern);
		                 SimpleDateFormat NewFormat = new SimpleDateFormat(NewPattern);              

		        //         System.out.println("Date saved in Global Variable: "+CCTimeTravelDate );
		                 Calendar cal = Calendar.getInstance();
		                 cal.setTime(CCTimeTravelDate);	               
		                 cal.add(Calendar.DATE, Integer.valueOf(Parameter_P));		                
		                 Date NewDate = cal.getTime();
		         //        System.out.println("New Date is: "+NewDate);		                 
		                 String ModifiedDate = "";		                 
		                 int GetDate = cal.DAY_OF_MONTH;
		                 if(GetDate<=9)
		                 {
		                	 ModifiedDate=NewFormat.format(NewDate);
		                 }
		                 else
		                	 ModifiedDate=Newformat.format(NewDate);
		        //         System.out.println("Modified date is: "+ModifiedDate);
		                
	            	  	String text = docHTML.body().text();
	            	  	Elements EditProfile=docHTML.body().getElementsByAttribute("href");
	            	  	for(int i=0;i<EditProfile.size();i++)	
	            	  	{if(EditProfile.get(i).text().contentEquals(MailTrapAccountDetailsContent))
	            	  		{
	           // 	  		System.out.println("Value of href is:"+EditProfile.get(i).text());
	           // 	  		System.out.println("URL is: "+EditProfile.get(i).attr("href"));
	            	  		Constants.URL=EditProfile.get(i).attr("href");
	            	  		break;
	            	  		}
	            	  	}
	            	  	String[] WordsToBeSearched={ModifiedDate,MailTrapAccountDetailsContent};
	            	  	String[] SearchResult=SearchOccurenceOfWordInExtireString(text,WordsToBeSearched);
	            	  	for(int SearchResultCounter=0;SearchResultCounter<SearchResult.length;SearchResultCounter++)
	            	  	{
	            	  		SearchResultToReturn+="Parameter: "+WordsToBeSearched[SearchResultCounter]+" Search Result: "+SearchResult[SearchResultCounter]+"\n";
	            	  	}
		            	  	if(SearchResultToReturn.contains("does not"))
		            	  	return Constants.KEYWORD_FAIL+ SearchResultToReturn;
		            	  	else
		            	  		return Constants.KEYWORD_PASS+ SearchResultToReturn;
		            	  }
				  }catch(Exception e){
					  e.printStackTrace();
				   return Constants.KEYWORD_FAIL+" -- Not able to find element "+e.getMessage();
				  }
		    }

	     
	     public String writeImportCSV(String object,String data) {
	 	    try {
	 		String[] p=data.split(",");
	 	    	String path = System.getProperty("user.dir")+"\\src\\ReferrerImportCSV.csv";
	 	    	Constants.ReferrerImportCSV=path;
	 	    	 Properties properties=LoadProperties();
	 		      String Answer=properties.getProperty("EmailUsedAsAccountNumber");
	 	       FileWriter writer = new FileWriter(path);
	 	       writer.write("Account Number");
	 	       writer.write('\n');
	 	       String AccountNumberType =properties.getProperty("AccountNumberType(MatchKeyOrPhoneNumber)");
	            if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("Matchkey"))
	              {
	              writer.write(Constants.MatchKey);
	              }
	            else if(Answer.equalsIgnoreCase("No")&&AccountNumberType.equalsIgnoreCase("PhoneNumber"))
	            
	            { 
	             writer.write(Constants.RefereePhoneNumber);
	            }
	            else
	            {
	             writer.write(Constants.Referrer);
	            }
	 	       writer.write(',');
	 	    	       String pattern = "MM/dd/yyyy";
	 	       SimpleDateFormat format = new SimpleDateFormat(pattern);
	 	       // formatting
	 	       String date=format.format(new Date());
	 	       String ModifiedDate = format.format(CCTimeTravelDate);
	 			           
	 	       writer.write(ModifiedDate);
	 	      
	 	       writer.write(',');
	 	       writer.write(p[0]);      
	 	       writer.flush();
	 	       writer.close();
	 	       
	 	       return Constants.KEYWORD_PASS;
	 	       } catch(Exception e){
	 				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	 	        }
	 	}
	     
	     
	    
	    public  String SetCCQATimeGeneralized(String object,String data)
		 {
	          APP_LOGS.debug("Setting QA time of the desired site");
	         
	          try{
	        	  Properties properties=LoadProperties();
	           String ControlCenterURL=properties.getProperty("ControlCenterURL");
	           String QATimeURL=ControlCenterURL+"/qaTime.jsp?action=set&time=";
	                 String pattern = "yyyy/MM/dd:HH:mm";
	                 SimpleDateFormat format = new SimpleDateFormat(pattern);
	                 // formatting
	                 String date=format.format(new Date());
	                 CalculateNewDate(object,data);  
	                 String ModifiedDate = format.format(CCTimeTravelDate);
	           driver.get(QATimeURL+ModifiedDate);
	          
	          }catch(Exception e){
	           return Constants.KEYWORD_FAIL+" Unable to open page "+e.getMessage();

	          }
	          return Constants.KEYWORD_PASS;
	          
	         }
	    
	    public Integer CalculateDays(String object,String data){  
			  APP_LOGS.debug("Calculate Days to be addded");
				  Properties properties=LoadProperties();
				  int DaysToBeAdded = 0;
				  String MasterList[]=data.split(",");
				  int MasterListLength= MasterList.length;
		          ArrayList<Integer> IntegerList= new ArrayList<Integer>();
		          ArrayList<String> StringList= new ArrayList<String>();
		          for(int i=0; i<MasterListLength; i++)
		          {
		        	  if(isInteger(MasterList[i])==true)
		        		  IntegerList.add(Integer.valueOf(MasterList[i]));
		        	  else
		        		  StringList.add(MasterList[i]);
		          }
		                 for(int StringIndex=0;StringIndex<StringList.size();StringIndex++)
				         {   if(StringList.get(StringIndex).contains("-"))
				        	 {
				        	 if(StringList.get(StringIndex).contains("/"))
				        		 DaysToBeAdded=DaysToBeAdded-(Integer.valueOf(properties.getProperty(StringList.get(StringIndex).split("-")[1].split("/")[0]))/(Integer.valueOf(StringList.get(StringIndex).split("/")[1])));
				        	 else
				        		 if(StringList.get(StringIndex).contains("*"))
					        		 DaysToBeAdded=DaysToBeAdded-(Integer.valueOf(properties.getProperty(StringList.get(StringIndex).split("-")[1].split("/")[0]))*(Integer.valueOf(StringList.get(StringIndex).split("/")[1])));
				        		 else DaysToBeAdded=DaysToBeAdded-Integer.valueOf(properties.getProperty(StringList.get(StringIndex).split("-")[1]));
				        	 }
				        
				         if(!StringList.get(StringIndex).contains("-"))
				         { if(StringList.get(StringIndex).contains("/"))
				        	 DaysToBeAdded=DaysToBeAdded+(Integer.valueOf(properties.getProperty(StringList.get(StringIndex).split("/")[0]))/(Integer.valueOf(StringList.get(StringIndex).split("/")[1])));
				        	 else
					        	 if(StringList.get(StringIndex).contains("*"))
					        	 DaysToBeAdded=DaysToBeAdded+(Integer.valueOf(properties.getProperty(StringList.get(StringIndex).split("/")[0]))*(Integer.valueOf(StringList.get(StringIndex).split("/")[1])));
					        	 else DaysToBeAdded=DaysToBeAdded+Integer.valueOf(properties.getProperty(StringList.get(StringIndex)));
				         }
				         }
		                 for(int IntegerIndex=0;IntegerIndex<IntegerList.size();IntegerIndex++)
				         {
		                	 DaysToBeAdded=DaysToBeAdded+IntegerList.get(IntegerIndex);
				         }
		                 return DaysToBeAdded;
			  }
	    
	    public String CalculateNewDate(String object,String data){  
			  APP_LOGS.debug("Calculating Date");
			  try{
			  
				  Calendar cal = Calendar.getInstance();
                  cal.add(Calendar.DATE, CalculateDays(object,data));
                  Date NewDate = cal.getTime(); 
                  CCTimeTravelDate =NewDate;
        //          System.out.println("New Date: "+CCTimeTravelDate);
			  }catch(Exception e){
				  e.printStackTrace();
			   return Constants.KEYWORD_FAIL+" -- Not able to calculate date";
			  }
			  return Constants.KEYWORD_PASS;
			 }
	    

	    
	    public String navigateToURLSavedInConst(String object,String data){  
			  APP_LOGS.debug("Navigating to URL");
			  try{
			  
			  driver.navigate().to(Constants.URL);
			  }catch(Exception e){
			   return Constants.KEYWORD_FAIL+" -- Not able to navigate";
			  }
			  return Constants.KEYWORD_PASS;
			 }
	    
	    public static boolean isInteger(String s) {
	        try { 
	            Integer.parseInt(s); 
	        } catch(NumberFormatException e) { 
	            return false; 
	        }
	        // only got here if we didn't return false
	        return true;
	    }
	    
	    public  String generateRandomRefereeEmail(String object,String data){
			APP_LOGS.debug("Writing in text box");
			
			try{
				String email="test"+Integer.toString((int)(Math.random()*10000))+"@test"+Integer.toString((int)(Math.random()*10000))+".com";
				driver.findElement(By.id(OR	.getProperty(object))).sendKeys(email);
				Constants.EmailSignup=email;			
			}catch(Exception e){
				return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
			}
			return Constants.KEYWORD_PASS;	
		}
	    
	    public String writeEmailForSearchReferee(String object,String data){
			 
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(Constants.EmailSignup);
	        	}
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to write "+e.getMessage();
	        	}
	     
			return Constants.KEYWORD_PASS;
		}
	    
	    public  String generateRandomEmail(String object,String data){
			APP_LOGS.debug("Writing in text box");
			
			try{
				String email="test"+Integer.toString((int)(Math.random()*10000))+"@test"+Integer.toString((int)(Math.random()*1000))+".com";
				driver.findElement(By.id(OR	.getProperty(object))).sendKeys(email);
				Constants.EmailSignUpReferrer=email;			
			}catch(Exception e){
				return Constants.KEYWORD_FAIL+" Unable to write "+e.getMessage();
			}
			return Constants.KEYWORD_PASS;	
		}
	    public  String ClickEditAccountLink(String object, String data){
		       APP_LOGS.debug("Clicking on link to write account information");
		       try{
		    	   Thread.sleep(10000L);
	    	   Properties properties=LoadProperties();
		            String Answer=properties.getProperty("MemberSiteURL");
		            List<WebElement> listOftagname_a = driver.findElements(By.tagName("a"));
		//            System.out.println("Size of elements- click edit accnt link: "+listOftagname_a.size());
		            for(int i=0; i<listOftagname_a.size(); i++) {
		            	if(listOftagname_a.get(i).getAttribute("href")!=null)
		            	{
		            		String subURL=listOftagname_a.get(i).getAttribute("href");
		         if(listOftagname_a.get(i).getAttribute("href").equalsIgnoreCase("profile.pg"))
		         {  
		        	 if(!Answer.endsWith("/"))
		        		 Answer=Answer+"/";
		//        	 System.out.println("URL: "+(Answer)+subURL);
		        	 driver.navigate().to((Answer)+subURL);
		        	 break;
		         }
		         else if(listOftagname_a.get(i).getAttribute("href").contains("profile.pg"))
		        	 {
		//        	 System.out.println("URL: "+subURL);
		        	driver.navigate().to(subURL);
		        	 break;
		        	 }

		        }
		            }
		        }
		        catch(Exception e){
		        	e.printStackTrace();
		        return Constants.KEYWORD_FAIL+" Unable to click "+e.getMessage();
		        }
		       return Constants.KEYWORD_PASS;
		       
		       
		      }
	    
	    public String clickEmailReferrer(String object,String data){
			 
	        APP_LOGS.debug("Clicking on link by link text");
	        try{
	        	String WaitElementResult=waitTilltextPartialLinkClickable(object,Constants.EmailSignUpReferrer);
	        	APP_LOGS.debug(WaitElementResult);
	//        	System.out.println("Referrer email address is: "+ Constants.EmailSignUpReferrer);
		         waitTilltextPartialLinkClickable(object,Constants.EmailSignUpReferrer);
		         String Parenthandle=driver.getWindowHandle();
		       Set<String> HandlesBeforeClick=driver.getWindowHandles();
		        int CurrentNoOfHandle=HandlesBeforeClick.size();
		        driver.findElement(By.partialLinkText(Constants.EmailSignUpReferrer)).click();
		        waitTillNoOfWindows(CurrentNoOfHandle+1);
		         //driver.close();
		         Set<String> handle=driver.getWindowHandles();
		        for(String winHandle : handle){  
		          
		          driver.switchTo().window(winHandle);
		        }
	        }
	        catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to click on link "+e.getMessage();
	        	}
	     
			return Constants.KEYWORD_PASS;
		}
	    
	    public String WriteMemberAddress(String object, String data)
	    {
	    	APP_LOGS.debug("Entering member's address details  ");
	        try{
	    	Properties properties=LoadProperties();
            String ClientRequiresAddress=properties.getProperty("ClientRequiresAddress");
            if (ClientRequiresAddress.equalsIgnoreCase("No"))
            	return Constants.KEYWORD_PASS+" -- Client does not require member's address";
            else
            {
               	String Address[]=data.split(",");
            	String Street1_Result=writeInInputFromColById("EditProfileStreetAdd1ID",Address[0]);
            	APP_LOGS.debug(Street1_Result);
            	String Street2_Result=writeInInputFromColById("EditProfileStreetAdd2ID",Address[1]);
            	APP_LOGS.debug(Street2_Result);
            	String City_Result=writeInInputFromColById("EditProfileCityID",Address[2]);
            	APP_LOGS.debug(City_Result);
            	String Country_Result=SelectItemFromDropdown("EditProfileCountryID",Address[3]);
            	APP_LOGS.debug(Country_Result);
            	String Click_Result=clickWebElementById("EditProfileCityID","");
            	APP_LOGS.debug(Click_Result);
            	String State_Wait_Result=waitTillObjectVisibilityID("EditProfileProvinceID","");
            	APP_LOGS.debug(State_Wait_Result);
            	String State_Result=SelectItemFromDropdown("EditProfileProvinceID",Address[4]);
            	APP_LOGS.debug(State_Result);            	
            	String Wait_Result=waitTillObjectVisibilityID("EditProfilePostalCodeID","");
            	APP_LOGS.debug(Wait_Result);
            	String PostalCode_Result=writeInInputFromColById("EditProfilePostalCodeID",Address[5]);
            	APP_LOGS.debug(PostalCode_Result);
            	return Constants.KEYWORD_PASS+" -- Member address details have been successfully entered";           
            }
	        }
            catch(Exception e){
				return Constants.KEYWORD_FAIL+" -- Not able to enter member's address details "+e.getMessage();
	        	}
	    }	    
	  
}
	 
	




